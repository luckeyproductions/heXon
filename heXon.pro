include(src/heXon.pri)

TARGET = hexon

LIBS += \
    $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \
    Dry/include/Dry/ThirdParty/Bullet

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

OTHER_FILES += \
    Docs/Todo.md \
    Resources/Settings.xml

DISTFILES += \
    LICENSE_TEMPLATE

unix
{
    isEmpty(DATADIR) DATADIR = $$(XDG_DATA_HOME)
    isEmpty(DATADIR) DATADIR = $$(HOME)/.local/share

    target.path = /usr/games/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/hexon/
    resources.files = Resources/*
    INSTALLS += resources

    icon.path = $$DATADIR/icons/
    icon.files = hexon.svg
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = hexon.desktop
    INSTALLS += desktop

    DEFINES += RESOURCEPATH=\\\"$${resources.path}\\\"
}
