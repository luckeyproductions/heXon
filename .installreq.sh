#!/bin/sh

sudo apt-get install \
    libx11-dev libxrandr-dev libasound2-dev \
    libegl1-mesa-dev libwayland-dev wayland-protocols \
    git make cmake build-essential qt5-default
