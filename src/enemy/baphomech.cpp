/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <Dry/ThirdParty/Bullet/BulletDynamics/ConstraintSolver/btHingeConstraint.h>

#include "../fx/mirage.h"
#include "../mastercontrol.h"
#include "../player/player.h"
#include "../player/ship.h"
#include "../projectile/brick.h"
#include "../projectile/seeker.h"
#include "../spawnmaster.h"
#include "razor.h"

#include "baphomech.h"

void Baphomech::RegisterObject(Context* context)
{
    context->RegisterFactory<Baphomech>();
}

Baphomech::Baphomech(Context* context): Enemy(context),
    barMaterial_{ RES(Material, "Materials/Flash.xml")->Clone() },
    spikeMaterial_{ RES(Material, "Materials/Baphoglow.xml") },
    engineSource_{ nullptr },
    healthBar_{ nullptr },
    spineRoot_{ nullptr },
    spine_{},
    spineOrigin_{},
    targetPosition_{},
    jawAngle_{},
    targetJawAngle_{},
    jawHitNormal_{},
    toSwitch_{},
    toShoot_{},
    maxShots_{},
    sinceShot_{},
    shotInterval_{},
    time_{}
{
}

void Baphomech::Start()
{
    Enemy::Start();

    meleeDamage_ = 2.3f;
    health_ = initialHealth_ = 420.f;
    worth_ = 666;

    engineSource_ = node_->CreateComponent<SoundSource3D>();
    engineSource_->SetDistanceAttenuation(42.f, 256.f, 2.f);
    engineSource_->SetSoundType(SOUND_EFFECT);
    engineSource_->SetGain(.55f);

    graphicsNode_ = node_->CreateChild("Graphics");
    AnimatedModel* head{ node_->CreateComponent<AnimatedModel>() };
    head->SetModel(RES(Model, "Models/BaphomechHead.mdl"));
    head->SetMaterial(0, RES(Material, "Materials/BlackEnvmap.xml"));
    head->SetMaterial(1, spikeMaterial_);
    head->SetMaterial(2, RES(Material, "Materials/Flesh.xml"));
    head->SetMaterial(3, spikeMaterial_->Clone());

    rigidBody_->SetMass(6.f);
    rigidBody_->SetFriction(0.f);
    rigidBody_->SetRollingFriction(0.f);
    rigidBody_->SetAngularFactor(Vector3::UP);
    rigidBody_->SetAngularRestThreshold(.01f);
    rigidBody_->SetAngularDamping(1.2f);

    node_->GetComponent<CollisionShape>()->SetSphere(6.f);

    for (int j{ 1 }; j <= 6; ++j)
    {
        Node* jawNode{ node_->GetChild("Jaw" + String{ j }, true) };

        if (!jawNode)
            break;

        jawNode->SetParent(node_);
        RigidBody* jawBody{ jawNode->CreateComponent<RigidBody>() };
        jawBody->SetMass(.333f);
        jawBody->SetFriction(0.f);
        jawBody->SetRollingFriction(0.f);

        jawBody->SetCollisionLayerAndMask(5, M_MAX_UNSIGNED);
        CollisionShape* jawShape{ jawNode->CreateComponent<CollisionShape>() };
        const float width{ .5f };
        const float length{ 6.f };
        jawShape->SetBox({ width, length, 2.f },
                         { 0.f, length * .5f - .23f, 0.f });

        Constraint* jawConstraint{ jawNode->CreateComponent<Constraint>() };
        jawConstraint->SetConstraintType(CONSTRAINT_HINGE);
        jawConstraint->SetDisableCollision(true);
        jawConstraint->SetOtherBody(rigidBody_);
        jawConstraint->SetWorldPosition(jawNode->GetWorldPosition());
        jawConstraint->SetAxis(Vector3::BACK);
        jawConstraint->SetOtherAxis(Vector3::UP);
        jawConstraint->SetOtherRotation(Quaternion{ 7.0f * (j % 2 ? 1.f : -1.f) + 120.f * ((j - 1) / 2), Vector3::UP } * Quaternion{ 90.f, Vector3::LEFT });
//        jawConstraint->SetRotation(Quaternion{ 8.0f, Vector3::UP });
        jawConstraint->SetLowLimit(Vector2::RIGHT  * (j % 2 ? -76.f :  0.f));//* (180.0f * ((j + 1) % 2) + 120.0f * ((j - 1) / 2.0f)));
        jawConstraint->SetHighLimit(Vector2::RIGHT * (j % 2 ?   0.f : 76.f));//* (180.0f * ((j + 1) % 2) + 120.0f * ((j - 1) / 2.0f)));
        jawConstraint->SetSoftness(0.f);
        jawConstraint->SetRelaxationFactor(0.f);
        jawConstraint->SetCFM(0.f);
        jawConstraint->SetERP(0.f);


        btHingeConstraint* btConstraint{ static_cast<btHingeConstraint*>(jawConstraint->GetConstraint()) };
        btConstraint->enableAngularMotor(true, 0.f, 100.f);

        SubscribeToEvent(jawNode, E_NODECOLLISIONSTART, DRY_HANDLER(Enemy, HandleNodeCollision));
    }

    spineOrigin_ = spineStartOrigin;
    spineRoot_ = node_->CreateChild("Spine");
    Node* vertebraNode{ spineRoot_ };
    StaticModelGroup* spineGroup{ spineRoot_->CreateComponent<StaticModelGroup>() };
    spineRoot_->CreateComponent<IKSolver>();

    spineGroup->SetModel(RES(Model, "Models/BaphomechSpine.mdl"));
    spineGroup->SetMaterial(spikeMaterial_);

    for (unsigned v{ 0u }; v < VERTEBRA_NUM; ++v)
    {
        vertebraNode = vertebraNode->CreateChild("Vertebra");
        vertebraNode->SetPosition(VERTEBRA_OFFSET * (v != 0u));

        Node* modelNode{ vertebraNode->CreateChild("Model") };
        spineGroup->AddInstanceNode(modelNode);
        modelNode->Rotate(Quaternion{ (v % 2u ? 23.f : -23.f) * v, Vector3::UP });

        if (spine_.Size() + 1u == VERTEBRA_NUM)
        {
            IKEffector* spineEffector{ vertebraNode->CreateComponent<IKEffector>() };
            spineEffector->SetChainLength(VERTEBRA_NUM - 1u);
        }

        spine_.Push(vertebraNode);
    }

    barMaterial_->SetShaderParameter("MatDiffColor", Color{ .666f, .17, 0.f, 1.f });
    healthBar_ = GetScene()->CreateChild("HealthBar");
    healthBar_->SetPosition(Vector3::FORWARD * (ARENA_RADIUS - .9f) + Vector3::DOWN * 2.3f);
    healthBar_->SetScale(Vector3{ ARENA_RADIUS, 1.f, 1.f });
//    StaticModel* barModel{ healthBar_->CreateComponent<StaticModel>() };
//    barModel->SetModel(RES(Model, "Models/Plane.mdl"));
//    barModel->SetMaterial(barMaterial_);
    healthBar_->CreateComponent<Mirage>();
}

void Baphomech::DrawDebug(StringHash, VariantMap&)
{
    DebugRenderer* debugRenderer{ GetScene()->GetComponent<DebugRenderer>() };

    rigidBody_->DrawDebugGeometry(debugRenderer, true);

    for (int j{ 1 }; j <= 6; ++j)
    {
        Node* jawNode{ node_->GetChild("Jaw" + String{ j }, true) };
        jawNode->GetComponent<RigidBody>()->DrawDebugGeometry(debugRenderer, true);
        jawNode->GetComponent<Constraint>()->DrawDebugGeometry(debugRenderer, false);
    }
}

void Baphomech::Set(const Vector3 &position)
{
    Enemy::Set(position);

    particleEmitter_->SetEmitting(false);
    healthBar_->SetEnabled(true);

    PlaySample(MC->GetSample("IMU"));
    MC->PlayBossMusic();

    for (int j{ 1 }; j <= 6; ++j)
    {
        Node* jawNode{ node_->GetChild("Jaw" + String{ j }, true) };
        SubscribeToEvent(jawNode, E_NODECOLLISION, DRY_HANDLER(Enemy, HandleNodeCollision));
    }

    toSwitch_ = 3;
    time_ = 0.f;
    maxShots_ = 0;
    toShoot_ = 0;
    sinceShot_ = 0.f;
    shotInterval_ = .17f;
    spineOrigin_ = spineStartOrigin;
    targetPosition_ = Vector3::ZERO;

    //    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Baphomech, DrawDebug));
}

void Baphomech::Disable()
{
    SceneObject::Disable();

    healthBar_->SetEnabled(false);
}

void Baphomech::HandleNodeCollision(StringHash eventType, VariantMap& eventData)
{
    using namespace NodeCollision;

    bool flank{ false };
    RigidBody* body{ static_cast<RigidBody*>(eventData[P_BODY].GetPtr()) };
    Node* node{ static_cast<Node*>(eventData[P_OTHERNODE].GetPtr()) };

    if (node && node->GetParent() == node_)
        return;

    MemoryBuffer contacts(eventData[P_CONTACTS].GetBuffer());
    while (!contacts.IsEof())
    {
        const Vector3 contactPosition{ (contacts.ReadVector3() - node_->GetWorldPosition()).Normalized() };

        if (Abs(contacts.ReadVector3().DotProduct(contactPosition)) < .333f)
        {
            flank = true;
            break;
        }
    }

    PODVector<RigidBody*> bodies{};
    node->GetComponent<RigidBody>()->GetCollidingBodies(bodies);

    if (flank)
    {
        for (RigidBody* b: bodies)
        {
            if (b != body && b->GetNode()->GetParent() == node_)
            {
                if (Ship* ship{ node->GetComponent<Ship>() })
                {
                    if (!ship->IsDiving())
                        ship->SetHealth(0.f);

                    return;
                }
                else if (Enemy* enemy{ node->GetDerivedComponent<Enemy>() })
                {
                    enemy->Hit(enemy->GetHealth(), 0);
                    return;
                }
            }
        }
    }

    Enemy::HandleNodeCollision(eventType, eventData);
}

void Baphomech::Update(float timeStep)
{
    Enemy::Update(timeStep);

    const float sinceShotFactor{ Clamp(sinceShot_ / shotInterval_, 0.f, 1.f) };
    const float spin{ rigidBody_->GetAngularVelocity().y_ };
    //Float like a float
    const float floatFactor{ (.5f - Min(.5f, .5f * Abs(node_->GetPosition().y_))) * 2.f };
    graphicsNode_->SetPosition(Vector3::UP * PowN(MC->Sine(0.17f, -floatFactor, floatFactor, sinceShotFactor * Sqrt(rigidBody_->GetLinearVelocity().Length()) * .005f), 3));
    graphicsNode_->SetRotation(Quaternion(Clamp(GetLinearVelocity().x_ * .333f, -6.f, 6.f),
                                          node_->WorldToLocal(Vector3::BACK)) *
                               Quaternion(Clamp(GetLinearVelocity().z_ * .333f, -6.f, 6.f),
                                          node_->WorldToLocal(Vector3::RIGHT)));

    Material* triangleMat{ node_->GetComponent<AnimatedModel>()->GetMaterial(3) };
    Color col{ triangleMat->GetShaderParameter("MatEmissiveColor").GetColor() };
    col.FromHSV(col.Hue() + timeStep * sinceShotFactor, 1.f - sinceShotFactor * .5f, .666f * (1.f - PowN(MC->Sine(2.f + toShoot_ * .25f, 0.f, 1.f, sinceShotFactor), 6)) * Max(.333f + .017f * (toShoot_ + Abs(rigidBody_->GetAngularVelocity().y_)), 1.f - sinceShotFactor * .666f));
    triangleMat->SetShaderParameter("MatEmissiveColor", col);
    Color spikeCol{};
    spikeCol.FromHSV(col.Hue(), col.SaturationHSV(), 1.f - col.Value());
    spikeMaterial_->SetShaderParameter("MatEmissiveColor", spikeCol);

    if (spine_.Size())
    {
        const Vector3 flatOrigin{ graphicsNode_->GetWorldPosition().Lerp(targetPosition_, timeStep * (GetLinearVelocity().Length() + 6.f)) };
        spineOrigin_ = spineOrigin_.Lerp(flatOrigin + Vector3::DOWN * spine_.Size() * 1.1f,
                                         timeStep * (1.f + (GetLinearVelocity().Length() + flatOrigin.Length())) * (sinceShot_ - shotInterval_));
        spineRoot_->SetWorldPosition(spineOrigin_);
        IKEffector* spineEffector{ spine_.Back()->GetComponent<IKEffector>() };
        spineEffector->SetTargetPosition(graphicsNode_->GetWorldPosition() + Vector3::DOWN);
        spineEffector->SetTargetRotation(Quaternion::IDENTITY);

        for (unsigned n{ 0 }; n < spine_.Size(); ++n)
        {
            Node* node{ spine_.At(n) };
            node->GetChild("Model")->Rotate(Quaternion{ timeStep * (spin * n * 6.f + 30.f + (6.f - n) * GetLinearVelocity().Length()
                                                        * spineOrigin_.DistanceToPoint(node->GetWorldPosition())), (n % 2 ? Vector3::UP : Vector3::DOWN) });
        }
    }

    const float danger{ Clamp(1.f - sinceShotFactor + PowN(spin, 2), 0.f, 1.f) };
    Vector3 barScale{ healthBar_->GetScale() };
    healthBar_->SetScale(Vector3{ barScale.x_,
                                  Lerp(barScale.y_, .1f, Clamp(timeStep *  6.f, 0.f, 1.f)),
                                  Lerp(barScale.z_, .1f, Clamp(timeStep * 12.f, 0.f, 1.f)) });
    healthBar_->GetComponent<Mirage>()->SetColor(Color{ Clamp( danger + (health_ + 1.f) / initialHealth_ - .333f * barScale.z_, 0.f, 1.f),
                                                        Min(spikeCol.MaxRGB() * .666f + barScale.z_ * .333f - danger * 0.17f, 1.f),
                                                        Clamp(col.MaxRGB() * spikeCol.MaxRGB() * danger - barScale.z_, 0.f, 1.f),
                                                        2.f - barScale.z_ });
}

void Baphomech::FixedUpdate(float timeStep)
{
    Player* player{ MC->GetNearestPlayer(node_->GetWorldPosition()) };

    const Vector3 velocity{ rigidBody_->GetLinearVelocity() };
    const float sinceShotFactor{ PowN(Clamp((sinceShot_ - PowN(shotInterval_, 2)) / shotInterval_, 0.f, 1.f), 2) };
    const float targetDistance{ node_->GetWorldPosition().DistanceToPoint(targetPosition_) };
    const bool centerTarget{ targetPosition_ == Vector3::ZERO };
    const bool shooting{ maxShots_ * toShoot_ != 0 && !centerTarget};
    UpdateJawMotors(timeStep);

    rigidBody_->ApplyForce(timeStep * VectorClamp(Min(6.f, PowN(targetDistance, 2)) * (targetPosition_ - node_->GetWorldPosition()).Normalized() - velocity, -Vector3::ONE, Vector3::ONE)
                           * 2342.f * rigidBody_->GetMass());

    Vector3 torque{};

    if (targetDistance > 2.f)
    {
        torque += (Min(PowN(Max(targetDistance - sinceShotFactor, 0.f), 6), 666.f)
                                 - rigidBody_->GetAngularVelocity().y_ * .05f) * Vector3::UP
                  * timeStep * 666.f * (.666f + maxShots_ * .111f);
    }

    const float trackFactor{ (!shooting
                ? 6.f * Clamp(1.f - PowN(targetDistance * .25f, 6), 0.f, 1.f)
                : Clamp(1.f - velocity.Length() * 6.f - PowN(sinceShot_, 4), 0.f, Max(1.f - shotInterval_ * 6.f, 0.f))) };

    if (trackFactor != 0.0f)
    {
        Vector3 playerPos{ node_->WorldToLocal(node_->GetWorldPosition() + Vector3::FORWARD) };

        if (shooting && player)
        {
            playerPos = node_->WorldToLocal(player->GetWorldPosition());
        }

        playerPos = Quaternion{ rigidBody_->GetAngularVelocity().y_ * rigidBody_->GetMass(), Vector3::UP } * playerPos;

        Vector3 nearest{ Vector3::ZERO };

        for (int h{ 0 }; h < 6; ++h)
        {
            Vector3 otherDirection{ Quaternion{ 60.f * h, Vector3::UP } * Vector3::FORWARD };
            if (otherDirection.DistanceToPoint(playerPos) < nearest.DistanceToPoint(playerPos))
                nearest = otherDirection;
        }

        const Vector3 angularVelocity{ rigidBody_->GetAngularVelocity() };

        torque += (nearest.CrossProduct(playerPos) * (90.f - angularVelocity.y_)) * timeStep * 666.f * trackFactor;
    }

    rigidBody_->ApplyTorque(torque);

    if (targetDistance < 1.f)
    {
      if (toSwitch_)
      {
          if (Abs(targetJawAngle_ - jawAngle_) < .1f)
          {
              if (toShoot_)
              {
                  if (sinceShot_ > shotInterval_)
                      Shoot();
              }
              else if (sinceShot_ > shotInterval_)
              {
                  targetJawAngle_ = 76.f * (targetJawAngle_ == 0.f);
                  --toSwitch_;
                  toShoot_ = Min(maxShots_, Random(Max(1, maxShots_ - 1)) + 2);
                  sinceShot_ = 0.f;

                  PlaySample(MC->GetSample("IMU2"), .05f);
                  PlaySample(MC->GetSample("IMU2_s"), .42f, false);
              }
          }
      }
      else if (sinceShot_ > (shotInterval_ * 3.f))
      {
          while (node_->GetWorldPosition().DistanceToPoint(targetPosition_) <= ARENA_RADIUS * .5f)
              targetPosition_ = Quaternion(Random(6) * 60.f, Vector3::UP) * Vector3::RIGHT * ARENA_RADIUS * .333f * Random(3);

          const float timeRoot{ Sqrt(time_ + 1.f) };

          toSwitch_ = 1 + Random(Max(2, + maxShots_ / 6) + (maxShots_ == 0));
          shotInterval_ = Lerp(.42f, .17f, Min(1.f, timeRoot * .17f));
          maxShots_ = Min(6, Floor(Max(0.f, .95f + time_ * .17f - timeRoot * 1.23f)));

          engineSource_->Play(MC->GetSample("Motor"));
      }
    }

    sinceShot_ += timeStep;
    time_ += timeStep;

    Razornado(timeStep);
}

void Baphomech::UpdateJawMotors(float timeStep)
{
    const float sinceShotFactor{ PowN(Clamp((sinceShot_ - PowN(shotInterval_, 2)) / shotInterval_, 0.f, 1.f), 2) };
    const float angle{ Lerp(jawAngle_, targetJawAngle_, timeStep * (.666f + .2f / shotInterval_) * 12.f * (sinceShotFactor == 1.f) ) - jawAngle_ };
    jawAngle_ += angle;

    for (int j{ 1 }; j <= 6; ++j)
    {
        Node* jawNode{ node_->GetChild("Jaw" + String{ j }, true) };
        Constraint* jawConstraint{ jawNode->GetComponent<Constraint>() };
        btHingeConstraint* btConstraint{ static_cast<btHingeConstraint*>(jawConstraint->GetConstraint()) };

        btConstraint->setMotorTargetVelocity((.666f + maxShots_ * shotInterval_) * 666.f * ((targetJawAngle_ == 0.f) ^ (j % 2) ? -1.f : 1.f));
    }
}

void Baphomech::Shoot()
{
    const bool seekerShot{ toShoot_ % 2 != 0 };
    sinceShot_ = 0.f;
    --toShoot_;

    const bool spawnRazors{ targetPosition_ == Vector3::ZERO && SPAWN->CountActive<Razor>() <= 20};

    for (int s{ 0 }; s < 6; ++s)
    {

        Vector3 direction{ Quaternion(60.f * s, Vector3::UP) * node_->GetWorldDirection() };
        const bool seekerPoint{ ((s % 2) ^ (targetJawAngle_ == 0.f)) != 0 };

        if (spawnRazors)
        {
            if (seekerPoint)
            {
                Razor* razor{ SPAWN->Create<Razor>() };
                razor->Set(spineOrigin_ + node_->GetWorldRotation().Inverse() * direction * (ARENA_RADIUS - 2.f));

                toShoot_ = maxShots_ > 4;
                maxShots_ = 0;
                toSwitch_ = 2;
            }
        }
        else
        {
            if (seekerPoint && seekerShot)
            {
                Seeker* seeker{ SPAWN->Create<Seeker>() };
                seeker->Set(node_->GetWorldPosition() + direction * 2.f, s < 2, true);
                seeker->SetLinearVelocity(direction * 23.f);
            }
            else if (!seekerPoint && !seekerShot)
            {
                Brick* brick{ SPAWN->Create<Brick>() };
                brick->Set(node_->GetWorldPosition() + direction * 2.f, direction, true);

                if (s < 2)
                {
                    PlaySample(MC->GetSample("Brick_s"), .14f, false);
                    PlaySample(MC->GetSample("Brick"), .34f);
                }
            }
        }
    }
}

void Baphomech::Razornado(float timeStep)
{
    for (Node* r: SPAWN->GetActive<Razor>())
    {
        RigidBody* razorBody{ r->GetComponent<RigidBody>() };
        const float orbit{ PowN(r->GetWorldPosition().Length() - ARENA_RADIUS * .9f, -3) };
        razorBody->ApplyForce((r->GetWorldPosition().CrossProduct(Vector3::UP) - r->GetWorldPosition().Normalized() * (1.7f + razorBody->GetLinearVelocity().LengthSquared() * .333f + orbit * .666f)).Normalized() * 666.f * timeStep);
    }
}

void Baphomech::Hit(float damage, int colorSet)
{
    Enemy::Hit(damage, colorSet);

    if (health_ == 0.f)
        healthBar_->SetEnabled(false);
    else
        healthBar_->SetScale(Vector3{ .666f * ARENA_RADIUS * (health_ / initialHealth_), .25f, healthBar_->GetScale().z_ + damage });
//    UpdateHealthBar();
}
