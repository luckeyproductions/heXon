/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef RAZOR_H
#define RAZOR_H

#include "enemy.h"

class Razor: public Enemy
{
    DRY_OBJECT(Razor, Enemy);

public:
    static void RegisterObject(Context* context);
    Razor(Context* context);

    void Start() override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void Set(const Vector3& position) override;

    void Hit(float damage, int ownerID) override;

protected:
    void Blink(const Vector3& newPosition, const Vector3& flashOffset = Vector3::ZERO) override;

    const float topSpeed_;
    float aimSpeed_;
    float spinRate_;

    Node* topNode_;
    Node* bottomNode_;
    StaticModel* topModel_;
    StaticModel* bottomModel_;
    Vector2 textureOffset;
    Vector2 textureOffsetDelta;
};

#endif // RAZOR_H
