/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../fx/mirage.h"

#include "apple.h"

void Apple::RegisterObject(Context *context)
{
    context->RegisterFactory<Apple>();
}

Apple::Apple(Context* context): Pickup(context)
{
}

void Apple::Start()
{
    Pickup::Start();

    node_->SetName("Apple");
    pickupType_ = PT_APPLE;
    initialPosition_ = Vector3::FORWARD * 10.8f;
    node_->SetPosition(initialPosition_);
    model_->SetModel(RES(Model, "Models/Apple.mdl"));
    model_->SetMaterial(RES(Material, "Materials/GoldEnvmap.xml"));

    Vector<ColorFrame> colorFrames;
    colorFrames.Push(ColorFrame{ Color{ .0f , .0f , .0f , .0f  }, .0f });
    colorFrames.Push(ColorFrame{ Color{ .55f, .55f, .23f, .55f }, .42f });
    colorFrames.Push(ColorFrame{ Color{ .0f , .0f , .0f , .0f  }, .8f });
    particleEmitter_->GetEffect()->SetColorFrames(colorFrames);

    graphicsNode_->CreateComponent<Mirage>()->SetColor(Color{ .6f, .5f, .23f, .9f });
}

void Apple::Update(float timeStep)
{
    Pickup::Update(timeStep);

    //Spin
    graphicsNode_->Rotate(Quaternion{ 0.f, 100.f * timeStep, 0.f });
    //Float like a float
    const float floatFactor{ .5f - Min(.5f, .5f * Abs(node_->GetPosition().y_)) };
    graphicsNode_->SetPosition(Vector3::UP * MC->Sine(.23f, -floatFactor, floatFactor, .23f));
}
