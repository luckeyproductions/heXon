/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../player/ship.h"

#include "chaoball.h"

void ChaoBall::RegisterObject(Context *context)
{
    context->RegisterFactory<ChaoBall>();
}

ChaoBall::ChaoBall(Context* context): Pickup(context)
{
}

void ChaoBall::Start()
{
    Pickup::Start();

    node_->SetName("ChaoBall");
    pickupType_ = PT_CHAOBALL;
    node_->SetRotation(Quaternion(Random(360.f), Random(360.f), Random(360.f)));
    initialPosition_ = Vector3::FORWARD * 5.f;
    node_->SetPosition(initialPosition_);
    model_->SetModel(RES(Model, "Models/Chaosphere.mdl"));
    model_->SetMaterial(RES(Material, "Materials/Chaosphere.xml")->Clone());

    rigidBody_->SetMass(3.f);

    Vector<ColorFrame> colorFrames;
    colorFrames.Push(ColorFrame{ Color{ .0f  , 1.f   , .0f  , 0.f   }, .0f  });
    colorFrames.Push(ColorFrame{ Color{ .23f ,  .05f , .5f  , 0.42f }, .17f });
    colorFrames.Push(ColorFrame{ Color{ .0f  ,  .0f  , .0f  , 0.f   }, .2f  });
    colorFrames.Push(ColorFrame{ Color{ .666f,  .0f  , .0f  , 1.f   }, .25f });
    colorFrames.Push(ColorFrame{ Color{ .0f  ,  .666f, .0f  , 1.f   }, .3f  });
    colorFrames.Push(ColorFrame{ Color{ .0f  ,  .0f  , .666f, 1.f   }, .35f });
    colorFrames.Push(ColorFrame{ Color{ .0f  ,  .0f  , .0f  , 0.f   }, .4f  });
    particleEmitter_->GetEffect()->SetColorFrames(colorFrames);
}

void ChaoBall::Update(float timeStep)
{
    Pickup::Update(timeStep);

    //Spin
    node_->Rotate(Quaternion{ 23.f  * timeStep,
                              100.f * timeStep,
                              42.f  * timeStep });
    //Float like a float
    const float floatFactor{ .5f - Min(.5f, .5f * Abs(node_->GetPosition().y_)) };

    graphicsNode_->SetPosition(Vector3::UP * MC->Sine(.5f, -floatFactor, floatFactor, .23f));

    Vector<Ship*> ships{ Ship::ships_ };

    for (Ship* ship: ships)
    {
        if (!ship->IsEnabled())
            ships.Remove(ship);
    }

    const Vector3 averageShipPos{ [&]{ auto average{ Vector3::ZERO };
                                      for (Ship* ship: ships)
                                          average += ship->GetWorldPosition();
                                      return average / Max(ships.Size(), 1); }() };

    if (IsEmerged() && MC->GetGameState() == GS_PLAY)
        rigidBody_->ApplyForce(-5.f * averageShipPos - rigidBody_->GetLinearVelocity());
}
