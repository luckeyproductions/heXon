﻿/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "inputmaster.h"
#include "settings.h"
#include "ui/menumaster.h"

#include "player/player.h"
#include "player/ship.h"

InputMaster::InputMaster(Context* context): Object(context),
    keyBindingsMaster_{},
    buttonBindingsMaster_{},
    keyBindingsPlayer_{},
    buttonBindingsPlayer_{},
    axisBindingsPlayer_{},
    pressedKeys_{},
    pressedJoystickButtons_{},
    leftStickPosition_{},
    rightStickPosition_{},
    controlledByPlayer_{}
{
    keyBindingsMaster_[KEY_UP]     = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_UP]    = MasterInputAction::UP;
    keyBindingsMaster_[KEY_DOWN]   = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_DOWN]  = MasterInputAction::DOWN;
    keyBindingsMaster_[KEY_LEFT]   = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_LEFT]  = MasterInputAction::LEFT;
    keyBindingsMaster_[KEY_RIGHT]  = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_RIGHT] = MasterInputAction::RIGHT;
    keyBindingsMaster_[KEY_LEFTBRACKET]  = keyBindingsMaster_[KEY_9] = keyBindingsMaster_[KEY_MINUS]  = keyBindingsMaster_[KEY_KP_MINUS] = MasterInputAction::DECREMENT;
    keyBindingsMaster_[KEY_RIGHTBRACKET] = keyBindingsMaster_[KEY_0] = keyBindingsMaster_[KEY_EQUALS] = keyBindingsMaster_[KEY_KP_PLUS]  = MasterInputAction::INCREMENT;
    buttonBindingsMaster_[CONTROLLER_BUTTON_A] = MasterInputAction::CONFIRM;
    keyBindingsMaster_[KEY_ESCAPE] = buttonBindingsMaster_[CONTROLLER_BUTTON_B]          = MasterInputAction::CANCEL;
    keyBindingsMaster_[KEY_P]      = buttonBindingsMaster_[CONTROLLER_BUTTON_START]      = MasterInputAction::PAUSE;
    keyBindingsMaster_[KEY_ESCAPE] = MasterInputAction::MENU;

    keyBindingsPlayer_[1][KEY_W] = MOVE_UP;
    keyBindingsPlayer_[1][KEY_S] = MOVE_DOWN;
    keyBindingsPlayer_[1][KEY_A] = MOVE_LEFT;
    keyBindingsPlayer_[1][KEY_D] = MOVE_RIGHT;
    // AZERTY
    keyBindingsPlayer_[1][KEY_Z] = MOVE_UP;
    keyBindingsPlayer_[1][KEY_Q] = MOVE_LEFT;

    keyBindingsPlayer_[2][KEY_UP]     = MOVE_UP;
    keyBindingsPlayer_[2][KEY_DOWN]   = MOVE_DOWN;
    keyBindingsPlayer_[2][KEY_LEFT]   = MOVE_LEFT;
    keyBindingsPlayer_[2][KEY_RIGHT]  = MOVE_RIGHT;

    keyBindingsPlayer_[1][KEY_KP_8]   = FIRE_N;
    keyBindingsPlayer_[1][KEY_KP_5]   = FIRE_S;
    keyBindingsPlayer_[1][KEY_KP_2]   = FIRE_S;
    keyBindingsPlayer_[1][KEY_KP_4]   = FIRE_W;
    keyBindingsPlayer_[1][KEY_KP_6]   = FIRE_E;
    keyBindingsPlayer_[1][KEY_KP_9]   = FIRE_NE;
    keyBindingsPlayer_[1][KEY_KP_3]   = FIRE_SE;
    keyBindingsPlayer_[1][KEY_KP_1]   = FIRE_SW;
    keyBindingsPlayer_[1][KEY_KP_7]   = FIRE_NW;
    keyBindingsPlayer_[1][KEY_I]      = FIRE_N;
    keyBindingsPlayer_[1][KEY_K]      = FIRE_S;
    keyBindingsPlayer_[1][KEY_J]      = FIRE_W;
    keyBindingsPlayer_[1][KEY_L]      = FIRE_E;
    keyBindingsPlayer_[1][KEY_LSHIFT] = RAM;
    keyBindingsPlayer_[1][KEY_CTRL]   = DEPTHCHARGE;
    keyBindingsPlayer_[1][KEY_ALT]    = REPEL;
    keyBindingsPlayer_[1][KEY_SPACE]  = DIVE;

    for (unsigned p: { 1u, 2u, 3u, 4u })
    {
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_A]  = RAM;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_B]  = REPEL;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_X]  = DIVE;
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_Y]  = DEPTHCHARGE;

        axisBindingsPlayer_[p][CONTROLLER_AXIS_LEFTX]  = PlayerInputAxis::MOVE_X;
        axisBindingsPlayer_[p][CONTROLLER_AXIS_LEFTY]  = PlayerInputAxis::MOVE_Y;
        axisBindingsPlayer_[p][CONTROLLER_AXIS_RIGHTX] = PlayerInputAxis::FIRE_X;
        axisBindingsPlayer_[p][CONTROLLER_AXIS_RIGHTY] = PlayerInputAxis::FIRE_Y;
    }

    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(InputMaster, HandleKeyDown));
    SubscribeToEvent(E_KEYUP, DRY_HANDLER(InputMaster, HandleKeyUp));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN, DRY_HANDLER(InputMaster, HandleJoystickButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONUP, DRY_HANDLER(InputMaster, HandleJoystickButtonUp));
    SubscribeToEvent(E_JOYSTICKAXISMOVE, DRY_HANDLER(InputMaster, HandleJoystickAxisMove));
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(InputMaster, HandleUpdate));
}

void InputMaster::HandleUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    //Mouse aim
    if (INPUT->GetNumJoysticks() == 0u)
    {
        const IntVector2 mm{ INPUT->GetMouseMove() };

        if (mm.Length() > 2.3f)
        {
            const Vector2 mouseDirection{ Vector2{ mm }.Normalized() * Vector2{ 1.f, -1.f } };
            rightStickPosition_[0] = mouseDirection;
        }
        else
        {
            rightStickPosition_[0] = {};
        }
    }

    //Clear actions
    InputActions activeActions{};
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        Vector<PlayerInputAction> emptyActions{};
        activeActions.player_[pId] = emptyActions;
    }

    //Convert key presses to actions
    for (int key: pressedKeys_)
    {
        //Check for master key presses
        if (keyBindingsMaster_.Contains(key))
        {
            MasterInputAction action{ keyBindingsMaster_[key] };
            if (!activeActions.master_.Contains(action))
                activeActions.master_.Push(action);
        }
        //Check for player key presses
        for (Player* p: MC->GetPlayers())
        {
            int pId{ p->GetPlayerId() };
            if (keyBindingsPlayer_[pId].Contains(key))
            {
                PlayerInputAction action{ keyBindingsPlayer_[pId][key] };
                if (!activeActions.player_[pId].Contains(action))
                    activeActions.player_[pId].Push(action);
            }
        }
    }
    //Check for joystick button presses
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        Vector<ControllerButton> pressedButtons{ pressedJoystickButtons_[pId - 1] };

        for (int button : pressedButtons)
        {
            //Check for master key presses
            if (buttonBindingsMaster_.Contains(button))
            {
                MasterInputAction action{ buttonBindingsMaster_[button] };
                if (!activeActions.master_.Contains(action))
                    activeActions.master_.Push(action);
            }

            if (buttonBindingsPlayer_[pId].Contains(button))
            {
                PlayerInputAction action{ buttonBindingsPlayer_[pId][button]};
                if (!activeActions.player_[pId].Contains(action))
                    activeActions.player_[pId].Push(action);
            }
        }
        //Handle eject
        if (pressedButtons.Contains(CONTROLLER_BUTTON_LEFTSHOULDER) && pressedButtons.Contains(CONTROLLER_BUTTON_RIGHTSHOULDER))
        {
            if (p->GetShip() && !MC->IsPaused())
                p->GetShip()->Eject();
        }
    }

    //Handle the registered actions
    HandleActions(activeActions);
}

void InputMaster::SetPlayerControl(int playerId, Controllable* controllable)
{

    if (controlledByPlayer_.Contains(playerId))
    {
        if (controlledByPlayer_[playerId] == controllable)
            return;
        else
            controlledByPlayer_[playerId]->ClearControl();
    }

    if (controllable != nullptr)
    {
        controlledByPlayer_[playerId] = controllable;
        controllable->HandleSetControlled();
    }
    else
    {
        controlledByPlayer_.Erase(playerId);
    }
}

Player* InputMaster::GetPlayerByControllable(Controllable* controllable) const
{
    for (int k: controlledByPlayer_.Keys())
    {
        Controllable* controlled{};
        if (controlledByPlayer_.TryGetValue(k, controlled) && controlled == controllable)
            return MC->GetPlayer(k);
    }

    return nullptr;
}

Controllable* InputMaster::GetControllableByPlayer(int playerId)
{
    return controlledByPlayer_[playerId];
}

void InputMaster::HandleActions(const InputActions& actions)
{
    MenuMaster* mm{ GetSubsystem<MenuMaster>() };
    const float t{ TIME->GetTimeStep() };

    //Handle master actions
    for (MasterInputAction action: actions.master_)
    {
        if (action == MasterInputAction::MENU &&
            (MC->GetGameState() == GS_LOBBY || MC->IsPaused()))
                SendEvent(E_MENUACTION);

        if (!mm->IsVisible())
            continue;

        switch (action) {
        case MasterInputAction::UP:        mm->PreviousElement();      break;
        case MasterInputAction::DOWN:      mm->NextElement();          break;
        case MasterInputAction::RIGHT:     mm->NextPage();             break;
        case MasterInputAction::LEFT:      mm->PreviousPage();         break;
        case MasterInputAction::DECREMENT: mm->Increment(-2.3f * t);   break;
        case MasterInputAction::INCREMENT: mm->Increment( 2.3f * t);   break;
        case MasterInputAction::CONFIRM:   mm->Confirm();              break;
        case MasterInputAction::CANCEL:    break;
        case MasterInputAction::PAUSE:     break;
        default: break;
        }
    }

    //Handle player actions
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        auto playerInputActions = actions.player_[pId];

        Controllable* controlled{ controlledByPlayer_[pId] };
        if (controlled && controlled->GetPlayer()->IsHuman())
        {
            if (!controlled->HasPath())
            {

                Vector3 stickMove{ leftStickPosition_[pId - 1].x_, 0.0f,  leftStickPosition_[pId - 1].y_ };
                Vector3 stickAim{ rightStickPosition_[pId - 1].x_, 0.0f, rightStickPosition_[pId - 1].y_ };

                if (stickMove.Length() < .17f)
                    stickMove *= .0f;

                if (stickAim.Length() < .42f)
                    stickAim *= .0f;

                controlled->SetMove(GetMoveFromActions(playerInputActions) + stickMove);
                controlled->SetAim(GetAimFromActions(playerInputActions) + stickAim);

                ControllableActions restActions{};
                restActions[RAM]         = playerInputActions->Contains(RAM);
                restActions[DIVE]        = playerInputActions->Contains(DIVE);
                restActions[DEPTHCHARGE] = playerInputActions->Contains(DEPTHCHARGE);
                restActions[REPEL]       = playerInputActions->Contains(REPEL);

                controlled->SetActions(restActions);
            }
        }

        //Move menu sliders
        MenuMaster* mm{ GetSubsystem<MenuMaster>() };
        if (!mm->IsVisible())
            continue;

        if (static_cast<int>(INPUT->GetNumJoysticks()) >= pId)
        {
            JoystickState* js{ INPUT->GetJoystick(pId - 1) };
            const float rt{ js->GetAxisPosition(CONTROLLER_AXIS_TRIGGERRIGHT) };
            const float lt{ js->GetAxisPosition(CONTROLLER_AXIS_TRIGGERLEFT) };
            const float triggers{ rt - lt };
            mm->Increment(5.f * t * triggers);
        }
    }
}

void InputMaster::HandleKeyDown(StringHash eventType, VariantMap &eventData)
{ (void)eventType;

    int key{eventData[KeyDown::P_KEY].GetInt()};

    if (!pressedKeys_.Contains(key))
        pressedKeys_.Push(key);

    switch (key){
    //Exit when ESC is pressed
    case KEY_ESCAPE:
        EjectButtonPressed(0);
        break;
    //Take screenshot when 9 is pressed
//    case KEY_9:
//    {
//        Screenshot();
//    } break;
    //Pause/Unpause game on P or joystick Start
    case KEY_P:
    {
        PauseButtonPressed();
    } break;
    }
}

void InputMaster::HandleKeyUp(StringHash eventType, VariantMap &eventData)
{ (void)eventType;

    int key{ eventData[KeyUp::P_KEY].GetInt() };

    if (pressedKeys_.Contains(key))
        pressedKeys_.Remove(key);
}

void InputMaster::HandleJoystickButtonDown(Dry::StringHash eventType, Dry::VariantMap& eventData)
{ (void)eventType;

    int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    JoystickState* joystickState{ INPUT->GetJoystickByIndex(joystickId) };
    ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonDown::P_BUTTON].GetInt()) };

    if (!pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Push(button);

    switch (button)
    {
    case CONTROLLER_BUTTON_START: PauseButtonPressed();
        break;
    case CONTROLLER_BUTTON_LEFTSHOULDER:
    case CONTROLLER_BUTTON_RIGHTSHOULDER:
        if (joystickState->GetButtonDown(CONTROLLER_BUTTON_LEFTSHOULDER)
         && joystickState->GetButtonDown(CONTROLLER_BUTTON_RIGHTSHOULDER))
            EjectButtonPressed(static_cast<int>(joystickId + 1));
        break;
    default: break;
    }
}

void InputMaster::HandleJoystickButtonUp(Dry::StringHash eventType, Dry::VariantMap& eventData)
{ (void)eventType;

    int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonUp::P_BUTTON].GetInt()) };

    if (pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Remove(button);
}

void InputMaster::HandleJoystickAxisMove(Dry::StringHash eventType, Dry::VariantMap& eventData)
{ (void)eventType;

    int joystickId{ eventData[JoystickAxisMove::P_JOYSTICKID].GetInt() };
    int axis{ eventData[JoystickAxisMove::P_AXIS].GetInt() };
    float position{ eventData[JoystickAxisMove::P_POSITION].GetFloat() };

    const HashMap<int, PlayerInputAxis>& axisBindings{ axisBindingsPlayer_[joystickId + 1] };
    PlayerInputAxis controlAxis{};
    if (axisBindings.TryGetValue(axis, controlAxis))
    {
        if (controlAxis == PlayerInputAxis::MOVE_X)
            leftStickPosition_[joystickId].x_ =  position;
        else if (controlAxis == PlayerInputAxis::MOVE_Y)
            leftStickPosition_[joystickId].y_ = -position;
        else if (controlAxis == PlayerInputAxis::FIRE_X)
            rightStickPosition_[joystickId].x_ =  position;
        else if (controlAxis == PlayerInputAxis::FIRE_Y)
            rightStickPosition_[joystickId].y_ = -position;
    }
}

void InputMaster::PauseButtonPressed()
{
    switch (MC->GetGameState())
    {
    case GS_INTRO: break;
    case GS_LOBBY: /*MC->SetGameState(GS_PLAY);*/ break;
    case GS_PLAY: MC->SetPaused(!MC->IsPaused()); break;
    case GS_DEAD: MC->SetGameState(GS_LOBBY); break;
    case GS_EDIT: break;
    default: break;
    }
}

void InputMaster::EjectButtonPressed(int playerId)
{
    if (MC->GetGameState() == GS_DEAD)
    {
        MC->SetGameState(GS_LOBBY);
        return;
    }
    else if (MC->GetGameState() != GS_PLAY || MC->IsPaused())
    {
        SendEvent(E_MENUACTION);
        return;
    }

    //Keyboard
    if (playerId == 0)
    {
        for (Ship* s : Ship::ships_)
        {
            if (s->IsEnabled())
            {
                s->Eject();
                return;
            }
        }
    }
}

Vector3 InputMaster::GetMoveFromActions(Vector<PlayerInputAction>* actions)
{
    return Vector3{ Vector3::RIGHT *
                (actions->Contains(MOVE_RIGHT) -
                 actions->Contains(MOVE_LEFT))

                + Vector3::FORWARD *
                (actions->Contains(MOVE_UP) -
                 actions->Contains(MOVE_DOWN)) };
}

Vector3 InputMaster::GetAimFromActions(Vector<PlayerInputAction>* actions)
{
    return Vector3{ Vector3::RIGHT *
                (actions->Contains(FIRE_E) -
                 actions->Contains(FIRE_W))

                +   Vector3::FORWARD *
                (actions->Contains(FIRE_N) -
                 actions->Contains(FIRE_S))
                + Quaternion(45.0f, Vector3::UP) *
                   (Vector3::RIGHT *
                (actions->Contains(FIRE_SE) -
                 actions->Contains(FIRE_NW))

                +   Vector3::FORWARD *
                (actions->Contains(FIRE_NE) -
                 actions->Contains(FIRE_SW))) };
}

void InputMaster::Screenshot()
{
    Graphics* graphics{ GetSubsystem<Graphics>() };
    Image screenshot{ context_ };
    graphics->TakeScreenShot(screenshot);
    //Here we save in the Data folder with date and time appended
    String fileName{ GetSubsystem<FileSystem>()->GetProgramDir() + "Screenshots/Screenshot_" +
            Time::GetTimeStamp().Replaced(':', '_').Replaced('.', '_').Replaced(' ', '_')+".png" };
    Log::Write(1, fileName);
    screenshot.SavePNG(fileName);
}
