/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../inputmaster.h"
#include "../hexonevents.h"
#include "player.h"

#include "controllable.h"

Controllable::Controllable(Context* context): SceneObject(context),
    path_{},
    move_{},
    aim_{},
    maxPitch_{ 90.f },
    minPitch_{  0.f },
    actions_{},
    actionInterval_{},
    actionSince_{},
    untilThought_{},
    model_{},
    rigidBody_{},
    collisionShape_{},
    animCtrl_{}
{
    actionInterval_[RAM]         = 1.7f;
    actionInterval_[DIVE]        = 3.4f;
    actionInterval_[DEPTHCHARGE] =  .8f;
    actionInterval_[REPEL]       = 2.3f;

    ChargeActions();

    SetUpdateEventMask(USE_UPDATE);
}

void Controllable::Start()
{
    SceneObject::Start();

    model_ = node_->CreateComponent<AnimatedModel>();
    rigidBody_ = node_->CreateComponent<RigidBody>();
    collisionShape_ = node_->CreateComponent<CollisionShape>();
    animCtrl_ = node_->CreateComponent<AnimationController>();

    model_->SetCastShadows(true);

    SubscribeToEvent(E_ENTERLOBBY, DRY_HANDLER(Controllable, EnterLobby));
    SubscribeToEvent(E_ENTERPLAY,  DRY_HANDLER(Controllable, EnterPlay));
}

void Controllable::Update(float timeStep)
{
    if (GetPlayer() == nullptr)
        return;

    for (unsigned a{ 0 }; a < actions_.size(); ++a)
            actionSince_[a] += timeStep;

    if (GetPlayer()->IsHuman() && !path_.Size())
    {
        return;
    }
    else
    {
        untilThought_ -= timeStep;

        if (untilThought_ < 0.f)
        {
            untilThought_ = Random(.023f, .042f);

            Think();
        }
    }
}

void Controllable::SetMove(Vector3 move)
{
    move = move.Normalized() * Pow(move.Length() * 1.05f, 2.f);

    if (move.Length() > 1.f)
        move.Normalize();

    move_ = move;
}

void Controllable::SetAim(Vector3 aim)
{
    aim = aim * (Vector3::ONE - Vector3::UP);

    aim_ = aim.Normalized();
}

void Controllable::ChargeActions()
{
    for (int a{ 0 }; a < NUM_ACTIONS; ++a)
        actionSince_[a] = actionInterval_[a];
}

void Controllable::SetActions(ControllableActions actions)
{
    if (actions == actions_)
    {
        return;
    }
    else
    {
        for (unsigned i{ 0 }; i < actions.size(); ++i)
        {
            if (actions[i] != actions_[i])
            {
                actions_[i] = actions[i];

                if (actions[i])
                    HandleAction(i);
            }
        }
    }
}

bool Controllable::HandleAction(int actionId)
{
    if (actionSince_[actionId] >= actionInterval_[actionId])
    {
        actionSince_[actionId] = 0.f;
        return true;
    }
    else
    {
        return false;
    }
}

void Controllable::AlignWithMovement(float timeStep)
{
    Quaternion rot{ node_->GetRotation() };
    Quaternion targetRot{};
    Vector3 direction{ .13f * move_ + rigidBody_->GetLinearVelocity() * Vector3{ 1.f, 0.f, 1.f }};

    if (direction.Length() < .1f)
        return;

    targetRot.FromLookRotation(direction);
    rot = rot.Slerp(targetRot, Clamp(timeStep * 5.f, 0.f, 1.f));
    node_->SetRotation(rot);
}

void Controllable::ClampPitch(Quaternion& rot)
{
    float maxCorrection{rot.EulerAngles().x_ - maxPitch_};

    if (maxCorrection > 0.f)
        rot = Quaternion{ -maxCorrection, node_->GetRight() } * rot;

    float minCorrection{rot.EulerAngles().x_ - minPitch_};

    if (minCorrection < 0.f)
        rot = Quaternion{ -minCorrection, node_->GetRight() } * rot;
}

void Controllable::ClearControl()
{
    path_.Clear();
    ResetInput();
}

void Controllable::Think()
{
    if (!path_.Size())
    {
        return;
    }
    else
    {
        const float toNextNode{ node_->GetPosition().DistanceToPoint(path_[0]) };

        if (toNextNode < (.1f + .34f * (path_.Size() > 1)))
            path_.Erase(0);
        else
            SetMove((path_[0] - node_->GetPosition()).Normalized());
    }
}

Player* Controllable::GetPlayer() const
{
    return GetSubsystem<InputMaster>()->GetPlayerByControllable(const_cast<Controllable*>(this));
}
