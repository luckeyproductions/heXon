/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../enemy/baphomech.h"
#include "../enemy/mason.h"
#include "../enemy/razor.h"
#include "../enemy/spire.h"
#include "../environment/arena.h"
#include "../fx/divebubbles.h"
#include "../fx/mirage.h"
#include "../fx/muzzle.h"
#include "../fx/phaser.h"
#include "../fx/repelring.h"
#include "../fx/soundeffect.h"
#include "../fx/trail.h"
#include "../pickup/apple.h"
#include "../pickup/chaoball.h"
#include "../pickup/coin.h"
#include "../pickup/heart.h"
#include "../pickup/pickup.h"
#include "../projectile/brick.h"
#include "../projectile/bullet.h"
#include "../projectile/chaoflash.h"
#include "../projectile/chaomine.h"
#include "../projectile/depthcharge.h"
#include "../projectile/seeker.h"
#include "ram.h"
#include "player.h"
#include "gui3d.h"
#include "pilot.h"

#include "ship.h"

Vector<Ship*> Ship::ships_{};

void Ship::RegisterObject(Context* context)
{
    context->RegisterFactory<Ship>();
    context->RegisterFactory<RamBow>();
    context->RegisterFactory<RepelRing>();
    context->RegisterFactory<DepthCharge>();
    context->RegisterFactory<DiveBubbles>();

    MasterControl* mc{ context->MC };
    mc->GetSample("Shot");
    mc->GetSample("Pickup1");
    mc->GetSample("Pickup2");
    mc->GetSample("Pickup3");
    mc->GetSample("Pickup4");
    mc->GetSample("Powerup");
    mc->GetSample("Chaos");
    mc->GetSample("ShieldHit");
    mc->GetSample("ShieldDown");

    for (int i{ 1 }; i < 5; ++i)
        mc->GetSample("SeekerHit" + String(i));
}

Ship::Ship(Context* context): Controllable(context),
    initialized_{ false },
    initialPosition_{},
    initialRotation_{},
    colorSet_{ 0 },
    initialHealth_{ 1.f },
    health_{ initialHealth_ },
    weaponLevel_{ 0 },
    bulletAmount_{ 1 },
    initialShotInterval_{ .30f },
    shotInterval_{initialShotInterval_},
    sinceLastShot_{ 0.f },
    hasRam_{   false },
    hasDive_{  false },
    hasMines_{ false },
    hasRepel_{ false },
    ramming_{ false },
    diving_{ false },
    appleCount_{ 0 },
    heartCount_{ 0 },
    diveBubbles_{ nullptr }
{
    thrust_ = 3000.f;
    maxSpeed_ = 23.f;

    ships_.Push(this);

    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

Ship::~Ship()
{
    ships_.Remove(this);
}

void Ship::Start()
{
    Controllable::Start();

//    MC->arena_->AddToAffectors(node_);

    PODVector<Node*> ships{};
    MC->scene_->GetChildrenWithComponent<Ship>(ships);
    colorSet_ = ships.Size();

    Node* guiNode{ MC->scene_->CreateChild("GUI3D") };
    gui3d_ = guiNode->CreateComponent<GUI3D>();
    gui3d_->Initialize(colorSet_);

    graphicsNode_ = node_->CreateChild("Graphics");
    model_ = graphicsNode_->CreateComponent<AnimatedModel>();
    model_->SetModel(RES(Model, "Models/KlaMk10.mdl"));

    shineEmitter_ = node_->CreateComponent<ParticleEmitter>();
    shineEmitter_->SetEmitting(false);

    CreateTails();
    SetTailsEnabled(false);

    shieldNode_ = graphicsNode_->CreateChild("Shield");
    shieldModel_ = shieldNode_->CreateComponent<StaticModel>();
    shieldModel_->SetModel(RES(Model, "Models/Shield.mdl"));
    shieldMaterial_ = RES(Material, "Materials/Shield.xml")->Clone();
    shieldModel_->SetMaterial(shieldMaterial_);

    muzzle_ = SPAWN->Create<Muzzle>(false);
    muzzle_->SetColor(colorSet_);

    //Setup ship physics
    rigidBody_->SetRestitution(.666f);
    rigidBody_->SetMass(0.f);
    rigidBody_->SetLinearFactor(Vector3::ONE - Vector3::UP);
    rigidBody_->SetLinearDamping(.5f);
    rigidBody_->SetAngularFactor(Vector3::ZERO);
    rigidBody_->SetLinearRestThreshold(.01f);
    rigidBody_->SetAngularRestThreshold(.1f);
    rigidBody_->SetCollisionLayerAndMask(1, M_MAX_UNSIGNED);

    collisionShape_->SetSphere(2.f);
    node_->CreateComponent<Navigable>();

    graphicsNode_->CreateComponent<Mirage>()->SetColor(MC->colorSets_[colorSet_].colors_.first_ * 1.23f);

    AddRam();
    AddDive();
    AddMines();
    AddRepel();
}

void Ship::Set(const Vector3& position, const Quaternion& rotation)
{
    if (!initialized_)
    {
        initialPosition_ = position;
        initialRotation_ = rotation;

        SetColors();

        initialized_ = true;
    }

    Controllable::Set(position, rotation);
}

void Ship::SetColors()
{
    model_->SetMaterial(0, MC->colorSets_[colorSet_].hullMaterial_);
    model_->SetMaterial(1, MC->colorSets_[colorSet_].glowMaterial_);

    SharedPtr<ParticleEffect> particleEffect{ CACHE->GetTempResource<ParticleEffect>("Particles/Shine.xml") };
    Vector<ColorFrame> colorFrames{};
    colorFrames.Push(ColorFrame(Color::TRANSPARENT_BLACK, 0.f));
    colorFrames.Push(ColorFrame(MC->colorSets_[colorSet_].colors_.first_ * .23f, .42f));
    colorFrames.Push(ColorFrame(Color::TRANSPARENT_BLACK, 1.2f));
    particleEffect->SetColorFrames(colorFrames);
    shineEmitter_->SetEffect(particleEffect);
}

void Ship::HandleSetControlled()
{
    GetPlayer()->gui3d_ = gui3d_;
    Player::takenColorSets_[GetPlayer()->GetPlayerId()] = colorSet_;
}

void Ship::EnterPlay(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!GetPlayer())
    {
        node_->SetEnabledRecursive(false);
        gui3d_->GetNode()->SetEnabledRecursive(false);
        return;
    }

    SetHealth(initialHealth_);
    GrabPickup(PT_RESET);

    rigidBody_->SetMass(1.f);
    rigidBody_->ApplyImpulse(node_->GetDirection() * 13.f);
    shineEmitter_->SetEmitting(true);

    SetTailsEnabled(true);

    PODVector<StaticModel*> models{};
    graphicsNode_->GetComponents<StaticModel>(models, true);
    models += model_;
    for (StaticModel* m: models)
        m->SetCastShadows(false);

//    for (int w{0};w<23;++w) /// Baphobattle
//        PowerupWeapons();
}

void Ship::EnterLobby(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    health_ = initialHealth_;
    weaponLevel_ = 0;
    bulletAmount_ = 1;
    gui3d_->SetBarrels(bulletAmount_);
    shotInterval_ = initialShotInterval_;
    sinceLastShot_ = 0.f;

    Set(initialPosition_, initialRotation_);
    model_->GetNode()->SetPosition(Vector3::ZERO);
    rigidBody_->SetMass(0.f);
    shineEmitter_->SetEmitting(false);

    shieldMaterial_->SetShaderParameter("MatDiffColor", Color::BLACK);

    SetTailsEnabled(false);
    EndRam();
    EndDive();
    graphicsNode_->SetObjectAnimation(nullptr);
    ChargeActions();

    PODVector<StaticModel*> models{};
    graphicsNode_->GetComponents<StaticModel>(models, true);
    models += model_;
    for (StaticModel* m: models)
        m->SetCastShadows(true);

    if (diveBubbles_)
        diveBubbles_->Clear();
}

void Ship::SetTailsEnabled(bool enable)
{
    for (Trail* t: tailGens_)
    {
        if (enable)
            t->ClearPointPath();

        t->SetEnabled(enable);
    }
}

void Ship::RemoveTails()
{
    for (Trail* t: tailGens_)
        t->GetNode()->Remove();

    tailGens_.Clear();
}

void Ship::CreateTails()
{
    RemoveTails();

    for (int t{ 0 }; t < 3; ++t)
    {
        const bool center{ t == 1 };
        Node* tailNode{ graphicsNode_->CreateChild("Tail") };
        tailNode->SetPosition({ -.85f + .85f * t, center ? 0.f : -.5f, center ? -.5f : -.23f });
        tailNode->SetRotation({ -30.f * t, Vector3::FORWARD });

        Trail* tailGen{ tailNode->CreateComponent<Trail>() };
        tailGen->SetMatchNodeOrientation(true);
        tailGen->SetDrawHorizontal(true);
        tailGen->SetDrawVertical(false);
        tailGen->SetNumTails(11);
        Color col{ MC->colorSets_[colorSet_].colors_.first_ };
//        col.FromHSV(col.Hue(), 1.f, 1.f);
        tailGen->SetColorForHead(col.Transparent(1/3.f));
        tailGen->SetColorForTip( col.Transparent(.0f));
        tailGens_.Push(tailGen);
    }
}

void Ship::ApplyMovement(float timeStep)
{
    const Vector3 force{ move_ * thrust_ * timeStep };

    if (rigidBody_->GetLinearVelocity().Length() < maxSpeed_
     || (rigidBody_->GetLinearVelocity().Normalized() + force.Normalized()).Length() < 1.f)
    {
        rigidBody_->ApplyForce(force);
    }
}

void Ship::FixedUpdate(float timeStep)
{
    if (rigidBody_->GetMass() != 0.f)
        ApplyMovement(timeStep);
}

void Ship::Update(float timeStep)
{

    if (MC->GetGameState() != GS_PLAY || !node_->IsEnabled())
        return;

    Controllable::Update(timeStep);

    //Update shield
    Quaternion shieldRotation{ Quaternion{ 0.f, TIME->GetElapsedTime() * 2000.f, 0.f } };
    shieldNode_->SetRotation(shieldNode_->GetRotation().Slerp(shieldRotation, Random(1.f)));
    Color shieldColor{ shieldMaterial_->GetShaderParameter("MatDiffColor").GetColor() };
    Color newColor { shieldColor.r_ * Random(.5f, .8f),
                     shieldColor.g_ * Random(.6f, .9f),
                     shieldColor.b_ * Random(.7f, .8f) };
    shieldMaterial_->SetShaderParameter("MatDiffColor", shieldColor.Lerp(newColor, Min(timeStep * 23.5f, 1.f)));

    //Float
    if (!diving_)
        model_->GetNode()->SetPosition(Vector3::UP * MC->Sine(.34f, -.1f, .1f));


    //Update rotation according to direction of the ship's movement.
    if (rigidBody_->GetLinearVelocity().Length() > .1f)
    {
        //        AlignWithMovement(timeStep);
        node_->LookAt(node_->GetPosition() + rigidBody_->GetLinearVelocity());
    }

    //Update tails
    float velocityToScale{ Clamp(.13f * rigidBody_->GetLinearVelocity().Length() - .1f, 0.f, 1.f) };

    for (Trail* tailGen: tailGens_)
    {
        const bool centerTail{ tailGen->GetNode()->GetPosition().x_ == 0.f };

        tailGen->SetTailLength(velocityToScale * (centerTail ? .34f : .17f));
        tailGen->SetWidthScale(velocityToScale * (centerTail ? .42f : .23f));
    }

    //Shooting
    sinceLastShot_ += timeStep;

    if (aim_.Length())
    {
        if (sinceLastShot_ > shotInterval_ && !IsDiving())
            Shoot(aim_);
    }

    //Attract coin
    for (Coin* c: Coin::EnabledCoins())
            c->GetComponent<RigidBody>()->ApplyForce((GetWorldPosition() - c->GetWorldPosition()).Normalized() * Pow(.5f, c->GetWorldPosition().DistanceToPoint(GetWorldPosition())) * 235e2f * timeStep);
}

void Ship::Shoot(const Vector3& aim)
{
    Player* player{ GetPlayer() };
    int maxBullets{ Min(Max(1, player->GetScore()), bulletAmount_) };

    for (int i{ 0 }; i < maxBullets; ++i)
    {
        float angle{ 0.f };
        switch (i) {
        case 0: angle = (maxBullets == 2 || maxBullets == 3) ? -5.f
                                                             :  0.f;
            break;
        case 1: angle = maxBullets < 4 ? 5.f
                                       : 7.5f;
            break;
        case 2: angle = maxBullets < 5 ? 180.f
                                       : 175.f;
            break;
        case 3: angle = -7.5f;
            break;
        case 4: angle = 185.f;
            break;
        default: break;
        }

        const Vector3 direction{ Quaternion{ angle, Vector3::UP } * aim };
        FireBullet(direction);
    }

    if (player->GetScore() == 0)
    {
        sinceLastShot_ = -.666f;
    }
    else
    {
        player->SetScore(player->GetScore() - maxBullets);
        sinceLastShot_ = 0.f;
    }

    //Create a single muzzle flash
    if (bulletAmount_ > 0)
    {
        MoveMuzzle();

        SoundEffect* shotSound{ SPAWN->Create<SoundEffect>() };
        shotSound->Set(node_->GetWorldPosition());
        shotSound->PlaySample(MC->GetSample("Shot"), .1f);
        PlaySample(MC->GetSample("Shot_s"), .14f, false);
    }
}

void Ship::FireBullet(const Vector3& direction)
{
    const Vector3 dir{ direction.Normalized() };
    const Vector3 position{ node_->GetPosition() + dir + Vector3::DOWN * .42f };
    const Vector3 force{ dir * (23.f + .42f * weaponLevel_) };
    const float damage{ .1f + .0023f * weaponLevel_ };

    SPAWN->Create<Bullet>()->Set(position, colorSet_, dir, force, damage);

}
void Ship::MoveMuzzle()
{
    muzzle_->Set(node_->GetPosition() + Vector3::DOWN * .42f);
}

void Ship::GrabPickup(PickupType pickup)
{
    if (health_ <= 0.f && pickup != PT_RESET)
        return;

    switch (pickup)
    {
    case PT_APPLE:
    {
        ++appleCount_;
        heartCount_ = 0;
        GetPlayer()->AddScore(23 * (1 + (3 * weaponLevel_ == 23)));

        if (appleCount_ >= 5)
        {
            PowerupWeapons();
            appleCount_ = 0;
        }
        else
        {
            PlayPickupSample(appleCount_);
        }
    }
    break;
    case PT_HEART:
    {
        ++heartCount_;
        appleCount_ = 0;

        if (heartCount_ >= 5)
        {
            PowerupShield();
            heartCount_ = 0;
        }
        else
        {
            SetHealth(Max(health_, Clamp(health_ + 5.f, 0.f, 10.f)));
            PlayPickupSample(heartCount_);
        }
    }
    break;
    case PT_CHAOBALL:
    {
        PickupChaoBall();
    }
    break;
    case PT_RESET:
    {
        appleCount_ = 0;
        heartCount_ = 0;
    }
    break;
    default: return;
    }

    GetPlayer()->gui3d_->SetHeartsAndApples(heartCount_, appleCount_);
}

bool Ship::HandleAction(int actionId)
{
    if (MC->GetGameState() != GS_PLAY || MC->IsPaused() || !IsEnabled())
        return false;

    Player* p{ GetPlayer() };
    unsigned cost{ 0 };
    switch (actionId)
    {
    case RAM:         cost = 3; break;
    case DIVE:        cost = 2; break;
    case DEPTHCHARGE: cost = 5; break;
    case REPEL:       cost = 1; break;
    default: break;
    }
    const unsigned currentScore{ p->GetScore() };
    if (cost > currentScore)
        return false;

    if (IsDiving() || !GetPlayer()->IsAlive())
        return false;

    if (!Controllable::HandleAction(actionId))
        return false;

    switch (actionId)
    {
    case RAM:         if (hasRam_)   Ram();         else return false; break;
    case DIVE:        if (hasDive_)  Dive();        else return false; break;
    case DEPTHCHARGE: if (hasMines_) DropDepthCharge(); else return false; break;
    case REPEL:       if (hasRepel_) Repel();       else return false; break;
    default: break;
    }

    p->SetScore(Max(0, currentScore - cost));

    return true;
}

void Ship::Ram()
{
//    Log::Write(LOG_INFO, "Ram activated");

    rigidBody_->ApplyImpulse(node_->GetWorldDirection() * 23.f);
    node_->CreateComponent<RamBow>()->SetDirection(node_->GetWorldDirection());

    PlaySample(RES(Sound, "Samples/Ram.wav"), .34f);
}

void Ship::Dive()
{
//    Log::Write(LOG_INFO, "Dive activated");

    ObjectAnimation* diveAnim{ new ObjectAnimation{ context_ } };
    ValueAnimation* posAnim{ new ValueAnimation{ context_ } };
    posAnim->SetKeyFrame(0.f, Vector3::ZERO);
    posAnim->SetKeyFrame(.25f, Vector3::DOWN * 5.f);
    posAnim->SetKeyFrame(.75f, Vector3::DOWN * 5.f);
    posAnim->SetKeyFrame(1.f, Vector3::ZERO);
    posAnim->SetInterpolationMethod(IM_SINUSOIDAL);
    posAnim->SetEventFrame(.95f, E_DIVED);
    ValueAnimation* rotAnim{ new ValueAnimation{ context_ } };
    rotAnim->SetKeyFrame(0.f, Quaternion::IDENTITY);
    rotAnim->SetKeyFrame(.1f, Quaternion{ 30.f, Vector3::RIGHT });
    rotAnim->SetKeyFrame(.25f, Quaternion::IDENTITY);
    rotAnim->SetKeyFrame(.75f, Quaternion::IDENTITY);
    rotAnim->SetKeyFrame(.9f, Quaternion{ -30.f, Vector3::RIGHT });
    rotAnim->SetKeyFrame(1.f, Quaternion::IDENTITY);
    rotAnim->SetInterpolationMethod(IM_SINUSOIDAL);
    diveAnim->AddAttributeAnimation("Position", posAnim, WM_ONCE, 2/3.f);
    diveAnim->AddAttributeAnimation("Rotation", rotAnim, WM_ONCE, 2/3.f);
    graphicsNode_->SetObjectAnimation(diveAnim);

    diving_ = true;
    rigidBody_->SetTrigger(true);
    SubscribeToEvent(graphicsNode_, E_DIVED, DRY_HANDLER(Ship, HandleDived));

    PlaySample(RES(Sound, "Samples/Dive.wav"), 1.f);

    EndRam();
}

void Ship::DropDepthCharge()
{
//    Log::Write(LOG_INFO, "Depth charge activated");

    DepthCharge* mine{ SPAWN->Create<class DepthCharge>() };
    mine->Set(node_->GetWorldPosition(), -node_->GetWorldDirection(), colorSet_);

    PlaySample(RES(Sound, "Samples/DepthCharge.wav"), .42f);
}

void Ship::Repel()
{
//    Log::Write(LOG_INFO, "Repel activated");

    const float radius{ 7.5f };
    PODVector<RigidBody*> bodies{};
    GetScene()->GetComponent<PhysicsWorld>()->GetRigidBodies(bodies, { node_->GetWorldPosition(), radius }, 1 + (1 << 2));
    for (RigidBody* rb: bodies)
    {
        if (rb == rigidBody_)
            continue;

        Node* otherNode{ rb->GetNode() };
        Ship* otherShip{ otherNode->GetComponent<Ship>() };
        if (otherShip && otherShip->IsDiving())
            continue;

        if (!Equals(rb->GetMass(), 0.f) && !otherNode->HasComponent<Brick>())
        {
            const bool isBaphomech{ otherNode->HasComponent<Baphomech>() };
            const Vector3 delta{ otherNode->GetWorldPosition() - node_->GetWorldPosition() };
            const float forceMagnitude{ 23.f * PowN(rb->GetMass(), 2 - isBaphomech) * Cbrt(Max(0.f, 1.f - delta.Length() / radius)) };
            rb->ApplyImpulse(delta.Normalized() * forceMagnitude);
        }
    }

    SPAWN->Create<RepelRing>()->Set(node_->GetWorldPosition(), colorSet_);
    PlaySample(RES(Sound, "Samples/Repel.wav"), .72f);
}

void Ship::PlayPickupSample(int pickupCount)
{
    SoundEffect* pickupSound{ SPAWN->Create<SoundEffect>() };
    pickupSound->Set(node_->GetWorldPosition());
    pickupSound->PlaySample(MC->GetSample("Pickup" + String(Clamp(pickupCount, 1, 4))), .23f);
}

void Ship::PowerupWeapons()
{
    if (weaponLevel_ < 23)
    {
        ++weaponLevel_;
        bulletAmount_ = 1 + ((weaponLevel_ + 5) / 6);
        shotInterval_ = initialShotInterval_ - .0042f * weaponLevel_;
        PlaySample(MC->GetSample("Powerup"), .42f, false);

        gui3d_->SetBarrels(bulletAmount_);
    }
    else
    {
        ///BOOM?
    }
}

void Ship::PowerupShield()
{
    SetHealth(15.f);
    PlaySample(MC->GetSample("Powerup"), .42f, false);
}

void Ship::PickupChaoBall()
{
    ChaoFlash* chaoFlash{ SPAWN->Create<ChaoFlash>() };
    chaoFlash->Set(MC->chaoBall_->GetWorldPosition(), colorSet_);

    PlaySample(MC->GetSample("Chaos_s"), .6f, false);
}

void Ship::SetHealth(float health)
{
    health_ = Clamp(health, 0.f, 15.f);
    GetPlayer()->gui3d_->SetHealth(health_);

    if (health_ <= 0.f)
        Explode();
}

void Ship::Hit(float damage, bool melee)
{
    if (health_ > 10.f)
    {
        damage *= (melee ? .75f : .25f);
        shieldMaterial_->SetShaderParameter("MatDiffColor", Color{ 2.f, 3.f, 5.f, .25f + .75f * (health_ - damage > 10.f) });
        PlaySample(MC->GetSample((health_ - damage) > 10.f ? "ShieldHit"
                                                           : "ShieldDown"), .23f);
    }
    else
    {
        if (!melee)
            PlaySample(MC->GetSample("SeekerHit" + String(Random(4) + 1)));
    }

    SetHealth(health_ - damage);
}

void Ship::Explode()
{
    GetPlayer()->Die();
    Disable();

    Explosion* explosion{ SPAWN->Create<Explosion>() };
    explosion->Set(node_->GetPosition(),
                   MC->colorSets_[colorSet_].colors_.first_,
                   2.f, 0);

    gui3d_->PlayDeathSound();

    GrabPickup(PT_RESET);

    for (Player* p: MC->GetPlayers()) // Check if last player to explode
        if (p->GetShip()->IsEnabled())
            return;

    MC->SetGameState(GS_DEAD);
}

void Ship::Eject()
{
    if (!IsEnabled() || MC->GetGameState() != GS_PLAY)
        return;

    SPAWN->Create<Phaser>()->Set(model_->GetModel(),
                                 graphicsNode_->GetWorldPosition(),
                                 rigidBody_->GetLinearVelocity() + node_->GetDirection() * 10e-5);
    Disable();
}

void Ship::Blink(const Vector3& newPosition, const Vector3& /*flashOffset*/)
{
    Phaser* phaser{ SPAWN->Create<Phaser>() };
    phaser->Set(model_->GetModel(), graphicsNode_->GetWorldPosition(), rigidBody_->GetLinearVelocity(), false, false);

    SceneObject::Blink(newPosition, graphicsNode_->GetPosition());
}

void Ship::Think()
{
    Vector3 move{ move_ };

    //Variation between autopilots
    float playerFactor{ 1.f };
    switch (GetPlayer()->GetPlayerId())
    {
    case 1: playerFactor = 2.3f; break;
    case 2: playerFactor = 3.4f; break;
    case 3: playerFactor = 6.66f; break;
    case 4: playerFactor = 5.f; break;
    }
    const float playerFactorInv{ 1.f / playerFactor };

    //Slight delay
    if (MC->GetSinceStateChange() < playerFactorInv)
    {
        move = Vector3::ZERO;
        return;
    }

    Vector3 pickupPos{ Vector3::ZERO };

    // Determine desired pickup location
    if (health_ < (5.f - appleCount_)
     || GetPlayer()->GetFlightScore() == 0
     || ((health_ + .5f * appleCount_) < 8.f)
     || (heartCount_ !=  0 && health_ <= 10.f && weaponLevel_ > 13)
     || (weaponLevel_== 23 && health_ <= 10.f)
     ||  heartCount_ ==  4)
    {
        pickupPos = MC->heart_->GetWorldPosition();
    }
    else
    {
        pickupPos = MC->apple_->GetWorldPosition();
    }

    //Calculate shortest route to desired pickup
    for (int i{ 0 }; i < 6; ++i)
    {
        const Vector3 projectedPickupPos{ pickupPos + (Quaternion(i * 60.f, Vector3::UP) * Vector3::FORWARD * ARENA_RADIUS * 2.f) };

        if (GetWorldPosition().DistanceToPoint(projectedPickupPos) < GetWorldPosition().DistanceToPoint(pickupPos))
            pickupPos = projectedPickupPos;
    }

    const float pickupDistanceFactor{  Clamp(1.f - (node_->GetPosition().DistanceToPoint(pickupPos) - playerFactorInv) * playerFactorInv, 0.f, 1.f) };
    move = (pickupPos - node_->GetPosition() - rigidBody_->GetLinearVelocity() * pickupDistanceFactor * playerFactorInv).Normalized();

    // Stay safe
    Vector3 fieldVector{ GetSubsystem<Arena>()->ForceFieldAt(node_->GetWorldPosition() + (move + rigidBody_->GetLinearVelocity()) * playerFactorInv) };
    fieldVector = fieldVector.ProjectOntoPlane(Vector3::UP) * fieldVector.y_;

    if (fieldVector.LengthSquared())
    {
        const float fieldEffect{ fieldVector.LengthSquared() * PowN(playerFactor, 5) + playerFactor };
        const Vector3 repelVector{ Max(0.f, -fieldVector.Normalized().DotProduct(node_->GetDirection() + move - fieldVector * fieldEffect) * playerFactor)
                                   * fieldEffect * fieldVector };

        move /= fieldEffect + 1.f;
        move += repelVector;

        const Vector3 orbitVector{ fieldEffect * Sign(fieldVector.DotProduct(node_->GetRight())) * Max(0.f, fieldVector.Normalized().DotProduct(move.Normalized()))
                                   * fieldVector.CrossProduct(Vector3::UP).Normalized() };

        move += orbitVector;
    }

    const float sway{ .1f * playerFactorInv *
                Max(0.f, 1.f - rigidBody_->GetLinearVelocity().LengthSquared() - fieldVector.LengthSquared()) *
                Min(1.f, GetWorldPosition().DistanceToPoint(pickupPos)) };
    move += Vector3{ MC->Sine(playerFactor, -sway, sway, playerFactor),
                     0.f,
                     MC->Sine(playerFactor, -sway, sway, -playerFactor) };

    SetMove(move.Normalized() * Sqrt(move.Length()));

    //Pick firing target
    bool fire{ false };
    Pair<float, Vector3> target{};

    for (Node* n: SPAWN->GetActive<Razor>())
    {
        Razor* r{ n->GetComponent<Razor>()};

        if (r->GetWorldPosition().y_ > (-playerFactor * .1f))
        {
            const float distance{ this->GetWorldPosition().DistanceToPoint(r->GetWorldPosition()) };
            const float panic{ r->GetPanic() };
            const float weight{ (5.f * panic) - (distance / playerFactor) + 42.f };

            if (weight > target.first_)
            {
                target.first_ = weight;
                target.second_ = r->GetWorldPosition() + r->GetLinearVelocity() * .42f;
                fire = true;
            }
        }
    }

    for (Node* n: SPAWN->GetActive<Spire>())
    {
        Spire* s { n->GetComponent<Spire>() };

        if (s->GetWorldPosition().y_ > (-playerFactor * .23f) && GetPlayer()->GetFlightScore() != 0)
        {
            const float distance{ this->GetWorldPosition().DistanceToPoint(s->GetWorldPosition()) };
            const float panic{ s->GetPanic() };
            const float weight{ (23.f * panic) - (distance / playerFactor) + 32.f };

            if (weight > target.first_)
            {
                target.first_ = weight;
                target.second_ = s->GetWorldPosition();
                fire = true;
            }
        }
    }

    for (Node* n: SPAWN->GetActive<Mason>())
    {
        Mason* m{ n->GetComponent<Mason>() };

        if (m->GetWorldPosition().y_ > (-playerFactor * .42f) && GetPlayer()->GetFlightScore() != 0)
        {
            const float distance{ this->GetWorldPosition().DistanceToPoint(m->GetWorldPosition()) };
            const float panic{ m->GetPanic() };
            const float weight{ (42.f * panic) - (distance / playerFactor) + 42.f };

            if (weight > target.first_)
            {
                target.first_ = weight;
                target.second_ = m->GetWorldPosition();
                fire = true;
            }
        }
    }

    for (Node* n: SPAWN->GetActive<Baphomech>())
    {
        Baphomech* b{ n->GetComponent<Baphomech>() };

        if (b->GetWorldPosition().y_ > (-playerFactor * .42f))
        {
            float weight{ 6.66f };

            if (weight > target.first_)
            {
                target.first_ = weight;
                target.second_ = b->GetWorldPosition();
                fire = true;
            }
        }
    }

    if (fire)
    {
        SetAim((target.second_ - GetWorldPosition()).Normalized());

        float aimFactor{ 23.f / playerFactor };

        if (bulletAmount_ == 2 || bulletAmount_ == 3)
        {
            SetAim((Quaternion((GetPlayer()->GetPlayerId() == 2 ? -1.f : 1.f)
                               * (Min(.666f * this->GetWorldPosition().DistanceToPoint(target.second_), 5.f) + MC->Sine(aimFactor * aimFactor, -aimFactor, aimFactor))
                               , Vector3::UP) * aim_).Normalized());
        }
        else
        {
            SetAim((Quaternion((GetPlayer()->GetPlayerId() == 2 ? -1.f : 1.f)
                                * MC->Sine(aimFactor * aimFactor, -aimFactor, aimFactor)
                                , Vector3::UP) * aim_).Normalized());
        }
    }
    else
    {
        SetAim(Vector3::ZERO);
    }
//    SetAim((aim_ - taste).Normalized());

    ControllableActions actions{};
    for (int a{ 0 }; a < 4; ++a)
    {
        if (CanUseAbility(a) && !Random(RoundToInt(235 * playerFactor)))
        {
            actions[a] = true;
            break;
        }
    }
    SetActions(actions);
}

bool Ship::HasAbility(int ability) const
{
    switch (ability)
    {
    case REPEL:       return hasRepel_;
    case DIVE:        return hasDive_;
    case RAM:         return hasRam_;
    case DEPTHCHARGE: return hasMines_;
    default: return false;
    }
}

bool Ship::CanUseAbility(int ability) const
{
    return IsEnabled() && GetPlayer()->IsAlive() && HasAbility(ability) && Equals(GetActionCharge(ability), 1.f);
}

void Ship::RemoveAbilities()
{
    hasRam_ = hasDive_ = hasMines_ = hasRepel_ = false;
    gui3d_->UpdateAbilityIcons(false);

    if (diveBubbles_)
        diveBubbles_->GetNode()->Remove();
}

void Ship::AddModule(int ability)
{
    String modelName{ "Models/"};
    switch (ability)
    {
    case RAM:         modelName += "Ram"; break;
    case DIVE:        modelName += "Dive"; break;
    case DEPTHCHARGE: modelName += "Mine"; break;
    case REPEL:       modelName += "Repel"; break;
    default: break;
    }
    modelName += "Module.mdl";

    Node* moduleNode{ graphicsNode_->CreateChild("Module") };
    StaticModel* module{ moduleNode->CreateComponent<StaticModel>() };
    module->SetModel(RES(Model, modelName));
    module->SetMaterial(0, MC->colorSets_[colorSet_].hullMaterial_);
    module->SetMaterial(1, MC->colorSets_[colorSet_].glowMaterial_);
    module->SetMaterial(2, RES(Material, "Materials/Gray.xml"));

    if (ability == DIVE)
    {
        diveBubbles_ = GetScene()->CreateChild("DiveBubbles")->CreateComponent<DiveBubbles>();
        diveBubbles_->SetShip(this);
    }

    gui3d_->UpdateAbilityIcons(false);
}

void Ship::HandleDived(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    EndDive();
}

void Ship::EndRam()
{
    ramming_ = false;

    RamBow* ram{ node_->GetComponent<RamBow>() };
    if (ram)
        ram->Remove();
}

void Ship::EndDive()
{
    diving_ = false;
    rigidBody_->SetTrigger(false);

    UnsubscribeFromEvent(E_DIVED);
}
