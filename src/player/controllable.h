/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CONTROLLABLE_H
#define CONTROLLABLE_H

#include "../sceneobject.h"
#include <bitset>

#define NUM_ACTIONS 4

typedef std::bitset<NUM_ACTIONS> ControllableActions;

class Player;

class Controllable: public SceneObject
{
    friend class InputMaster;
    DRY_OBJECT(Controllable, SceneObject);

public:
    Controllable(Context* context);

    void Start() override;
    void Update(float timeStep) override;

    void SetMove(Vector3 move);
    void SetAim(Vector3 aim);
    void SetActions(ControllableActions actions);

    virtual void HandleSetControlled() {}
    virtual void ClearControl();
    bool HasPath() const { return path_.Size() > 0; }

    virtual void EnterLobby(StringHash /*eventType*/, VariantMap& /*eventData*/) {}
    virtual void EnterPlay(StringHash /*eventType*/, VariantMap& /*eventData*/) {}
    virtual void Think();

    Player* GetPlayer() const;
    float GetActionCharge(int actionId) const { return Min(1.f, *actionSince_[actionId] / *actionInterval_[actionId]); }

    void ChargeActions();

protected:
    void ResetInput() { move_ = aim_ = Vector3::ZERO; actions_.reset(); }
    void ClampPitch(Quaternion& rot);
    void AlignWithMovement(float timeStep);
    virtual bool HandleAction(int actionId);

    PODVector<Vector3> path_;
    Vector3 move_;
    Vector3 aim_;
    float thrust_;
    float maxSpeed_;
    float maxPitch_;
    float minPitch_;

    ControllableActions actions_;
    HashMap<int, float> actionInterval_;
    HashMap<int, float> actionSince_;
    float untilThought_;

    AnimatedModel* model_;
    RigidBody* rigidBody_;
    CollisionShape* collisionShape_;
    AnimationController* animCtrl_;

};

#endif // CONTROLLABLE_H
