/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef RAM_H
#define RAM_H

#include "../luckey.h"


class RamBow: public LogicComponent
{
    DRY_OBJECT(RamBow, LogicComponent);

public:
    RamBow(Context* context);

    void OnSetEnabled() override;
    void Start() override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void Stop() override;

    void SetDirection(const Vector3& dir) { dir_ = dir; }

    Node* innerNode_;
    Node* outerNode_;
    SharedPtr<Material> ramMatInner_;
    SharedPtr<Material> ramMatOuter_;
    float ramTime_;
    Vector3 dir_;
};

#endif // RAM_H
