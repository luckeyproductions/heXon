/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../enemy/enemy.h"
#include "ship.h"

#include "ram.h"

RamBow::RamBow(Context* context): LogicComponent(context),
    innerNode_{ nullptr },
    outerNode_{ nullptr },
    ramMatInner_{ nullptr },
    ramMatOuter_{ nullptr },
    ramTime_{ 0.f }
{
    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void RamBow::OnSetEnabled()
{
    LogicComponent::OnSetEnabled();

    if (IsEnabled())
    {
        ramTime_ = 0.f;
        innerNode_->Roll(Random(360.f));
        outerNode_->Roll(Random(360.f));
    }
}

void RamBow::Start()
{
    innerNode_ = node_->CreateChild("RamInner");
    outerNode_ = node_->CreateChild("RamOuter");
    outerNode_->SetScale(1.1f);

    Color innerCol{ MC->colorSets_[node_->GetComponent<Ship>()->GetColorSet()].colors_.first_ };
    ramMatInner_ = RES(Material, "Materials/Ram.xml")->Clone();
    ramMatInner_->SetShaderParameter("MatDiffColor", innerCol);
    Color outerCol{ MC->colorSets_[node_->GetComponent<Ship>()->GetColorSet()].colors_.second_ };
    ramMatOuter_ = RES(Material, "Materials/Ram.xml")->Clone();
    ramMatOuter_->SetShaderParameter("MatDiffColor", outerCol);

    for (Node* n: { innerNode_, outerNode_ })
    {
        StaticModel* layer{ n->CreateComponent<StaticModel>() };
        layer->SetModel(RES(Model, "Models/Ram.mdl"));
        if (n == innerNode_)
            layer->SetMaterial(ramMatInner_);
        else
            layer->SetMaterial(ramMatOuter_);
    }

}

void RamBow::Update(float timeStep)
{
    if (!IsEnabled())
        return;

    LogicComponent::Update(timeStep);

    float spin{ 235.f };
    innerNode_->Roll( spin * timeStep);
    outerNode_->Roll(-spin * timeStep);
    ramTime_ += timeStep;

    const float alpha{ 1.7f * (5.f * ramTime_ - PowN(2.3f * ramTime_, 2)) - dir_.Angle(node_->GetWorldDirection()) * .1f };
    for (Material* m: { ramMatInner_, ramMatOuter_ })
    {
        Color col{ m->GetShaderParameter("MatDiffColor").GetColor() };
        m->SetShaderParameter("MatDiffColor", col.Transparent(.55f * Min(1.f, alpha)));
    }

    if (ramTime_ > 0.f && alpha <= 0.f)
        node_->GetComponent<Ship>()->EndRam();
}

void RamBow::FixedUpdate(float /*timeStep*/)
{
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };

    PODVector<RigidBody*> bodies{};
    physics->GetRigidBodies(bodies, Sphere{ node_->GetWorldPosition() + node_->GetWorldDirection(), 2/3.f });
    for (RigidBody* rb: bodies)
    {
        if (rb != node_->GetComponent<RigidBody>())
        {
            if (Enemy* e = rb->GetNode()->GetDerivedComponent<Enemy>())
                e->Hit(Min(17.f, e->GetHealth()), node_->GetComponent<Ship>()->GetColorSet());
        }
    }
}

void RamBow::Stop()
{
    innerNode_->Remove();
    outerNode_->Remove();
}
