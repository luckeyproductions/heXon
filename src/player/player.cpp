/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../inputmaster.h"
#include "../spawnmaster.h"
#include "../hexocam.h"
#include "../environment/arena.h"
#include "../environment/door.h"
#include "../environment/splatterpillar.h"
#include "../fx/explosion.h"
#include "../fx/muzzle.h"
#include "../fx/phaser.h"
#include "../pickup/apple.h"
#include "../pickup/chaoball.h"
#include "../pickup/heart.h"
#include "../projectile/bullet.h"
#include "../projectile/chaoflash.h"
#include "pilot.h"
#include "ship.h"
#include "gui3d.h"

#include "player.h"

HashMap<int, int> Player::takenColorSets_{};

Player::Player(int playerId, Context* context): Object(context),
    gui3d_{ nullptr },
    playerId_{ playerId },
    autoPilot_{ playerId_ != 1 && !INPUT->GetJoystickByIndex(playerId_ - 1) },
//    autoPilot_{ false },
//    autoPilot_{ true },
    alive_{ false },
    score_{ 0u },
    flightScore_{ 0u },
    multiplier_{ 0u }
{
    SubscribeToEvent(E_ENTERLOBBY, DRY_HANDLER(Player, EnterLobby));
    SubscribeToEvent(E_ENTERPLAY,  DRY_HANDLER(Player, EnterPlay));
}

void Player::Die()
{
    alive_ = false;
}

void Player::Respawn()
{
    ResetScore();

    if (takenColorSets_.Contains(playerId_))
    {
//        MC->GetShipByColorSet(takenColorSets_[playerId_])->RemoveAbilities();
        takenColorSets_.Erase(playerId_);
    }

    alive_ = true;
}

void Player::SetScore(int points)
{
    score_ = points;

    if (gui3d_ != nullptr)
        gui3d_->SetScore(score_);

}

void Player::ResetScore()
{
    SetScore(0);
}

void Player::EnterPlay(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    flightScore_ = 0;
}

void Player::EnterLobby(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    EnterLobby();
}

void Player::EnterLobby()
{
    multiplier_ = 0u;

    for (Pilot* pilot: MC->GetComponentsInScene<Pilot>(false))
    {
        if (playerId_ == pilot->GetPlayerId())
        {
            GetSubsystem<InputMaster>()->SetPlayerControl(playerId_, pilot);

            if (!alive_)
                pilot->Revive();
            else
                pilot->EnterLobbyFromShip();
        }
    }
}

void Player::AddScore(int points)
{
    if (!alive_ || points == 0)
        return;

    points *= PowN(2, multiplier_);
    SetScore(GetScore() + points);

    //Check for multiplier increase
    for (int i{ 0u }; i < 10u; ++i)
    {
        unsigned tenPow{ PowN(10u, i) };

        if (flightScore_ < tenPow && (flightScore_ + points) >= tenPow)
        {
            ++multiplier_;
            GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_)->
                    PlaySample(MC->GetSample("MultiX"), .42f);

            Ship* ship{ GetShip() };

            if (ship)
                MC->arena_->FlashX(MC->colorSets_[ship->GetColorSet()].colors_.first_);

            break;
        }
    }

    flightScore_ += points;
}

Vector3 Player::GetWorldPosition()
{
    return GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_)->GetWorldPosition();
}

Ship* Player::GetShip()
{
    Controllable* controllable{ GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_) };
    if (controllable->IsInstanceOf<Ship>())
    {
        Ship* ship{ static_cast<Ship*>(controllable) };
        return ship;
    }

    return nullptr;
}
