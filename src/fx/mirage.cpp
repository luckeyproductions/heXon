/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mirage.h"
#include "../mastercontrol.h"
#include "../environment/arena.h"

void Mirage::RegisterObject(Context* context)
{
    context->RegisterFactory<Mirage>();
}

Mirage::Mirage(Context* context): AnimatedBillboardSet(context),
    billboardSet_{},
    color_{ .42f, .42f, .42f, .42f },
    size_{ 1.f },
    fade_{ 1.f }
{
}

void Mirage::OnNodeSet(Node* node)
{
    if (!node)
        return;

    AnimatedBillboardSet::OnNodeSet(node);

    SetNumBillboards(2);
    SetMaterial(RES(Material, "Materials/Mirage.xml"));
    SetSorted(true);
    SetRelative(true);

    for (Billboard& bb: GetBillboards())
    {
//        bb.size_ = Vector2::ONE;
//        bb.position_ = Vector3::ZERO;
        bb.color_ = Color::TRANSPARENT_BLACK;
        bb.enabled_ = true;
    }

    LoadFrames(RES(XMLFile, "Textures/Mirage.xml"));
    Commit();

}

void Mirage::UpdateGeometry(const FrameInfo& frame)
{
    const Vector3 normalizedPosition{ node_->GetWorldPosition().Normalized() };

    Pair<Vector3, Vector3> hexants(-MC->GetHexant(Quaternion{ -30.f, Vector3::UP } * normalizedPosition),
                                   -MC->GetHexant(Quaternion{  30.f, Vector3::UP } * normalizedPosition)
                                  );

    for (unsigned b{ 0u }; b < GetNumBillboards(); ++b)
    {
        Billboard& bb{ GetBillboards()[b] };

        const float radius{ ARENA_RADIUS };
        const Vector3 offset{ (b == 0 ? hexants.first_ : hexants.second_) * 2.f * radius };
        bb.position_ = (Vector3::ONE / node_->GetWorldScale()) * (node_->GetWorldRotation().Inverse() * (offset + (node_->GetWorldPosition() + offset) * .005f));
        float intensity{ Clamp(1.f - ((node_->GetWorldPosition() + offset).ProjectOntoAxis(offset.Normalized()) - radius) * .3f, 0.f, 1.f) };

        if (hexants.first_ == hexants.second_ && b > 0)
            intensity = 0.f;

        bb.color_ = intensity * intensity * color_;
        const float squish{ intensity * Clamp(node_->GetWorldPosition().z_ / radius, 0.f, 1.f) };
        bb.size_ = size_ * Vector2::ONE * (1.5f - .5f * intensity) - .23f * Vector2::UP * squish * squish * squish * squish;
    }

    const bool enabled{ node_->IsEnabled() };

    if (IsEnabled() != enabled)
        SetEnabled(enabled);

    AnimatedBillboardSet::UpdateGeometry(frame);
}
