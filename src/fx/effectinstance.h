/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef EFFECTINSTANCE_H
#define EFFECTINSTANCE_H

#include "effect.h"

class EffectInstance : public Effect
{
    DRY_OBJECT(EffectInstance, Effect);

public:
    static void RegisterObject(Context* context);
    EffectInstance(Context* context);

    virtual void Set(const Vector3& position, ParticleEffect* effect);
};

#endif // EFFECTINSTANCE_H
