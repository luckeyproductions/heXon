/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../inputmaster.h"
#include "../player/player.h"

#include "phaser.h"

void Phaser::RegisterObject(Context* context)
{
    context->RegisterFactory<Phaser>();
    context->MC->GetSample("Flash");
}

Phaser::Phaser(Context* context): Effect(context),
    staticModel_{},
    phaseMaterial_{ RES(Material, "Materials/Phase.xml")->Clone() },
    velocity_{},
    spin_{},
    stateChanger_{}
{
}

void Phaser::Start()
{
    Effect::Start();

    node_->SetName("Phaser");
    staticModel_ = node_->CreateComponent<StaticModel>();
}

void Phaser::Set(Model* model, const Vector3& position, const Vector3& velocity, bool stateChanger, bool audible)
{
    stateChanger_ = stateChanger;

    Effect::Set(position);

    velocity_ = velocity;
    spin_ = Quaternion::IDENTITY;

    node_->LookAt(position + velocity);

    staticModel_->SetModel(model);
    staticModel_->SetMaterial(phaseMaterial_);

    phaseMaterial_->SetShaderParameter("Dissolve", 0.f);

    if (audible)
        PlaySample(MC->GetSample("Flash"), .23f);
}

void Phaser::Set(Model* model, const Vector3& position, const Quaternion& rotation, const Vector3& velocity, const Quaternion& spin)
{
    stateChanger_ = false;

    Effect::Set(position);
    node_->SetRotation(rotation);

    velocity_ = velocity;
    spin_ = spin;

    staticModel_->SetModel(model);
    staticModel_->SetMaterial(phaseMaterial_);

    phaseMaterial_->SetShaderParameter("Dissolve", 0.f);
}

void Phaser::Update(float timeStep)
{
    Effect::Update(timeStep);

    node_->Translate(velocity_ * timeStep, TS_WORLD);
    node_->Rotate(spin_);

    float dissolveValue { Clamp(age_ * 2.3f, 0.f, 1.f) };
    phaseMaterial_->SetShaderParameter("Dissolve", dissolveValue);

    if (!stateChanger_)
    {
        if (dissolveValue >= 1.f)
            Disable();

        velocity_   = velocity_.Lerp(Vector3::ZERO, timeStep + age_ * .23f);
        spin_       = spin_.Slerp(Quaternion::IDENTITY, timeStep + age_ * .23f);
    }

    if (age_ > 2.f)
    {
        if (stateChanger_)
        {
            for (Controllable* c : GetSubsystem<InputMaster>()->GetControlled())
            {
                if (c->IsEnabled())
                    return;
            }

            MC->SetGameState(GS_LOBBY);
        }

        Disable();
    }
}
