/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "borderline.h"

BorderLine::BorderLine(Context* context, unsigned char drawableFlags): Drawable(context, drawableFlags)
{
}

void BorderLine::OnSetEnabled() { Drawable::OnSetEnabled(); }
void BorderLine::UpdateBatches(const FrameInfo& frame) { Drawable::UpdateBatches(frame); }
void BorderLine::UpdateGeometry(const FrameInfo& frame) {}

UpdateGeometryType BorderLine::GetUpdateGeometryType()
{
    return UPDATE_MAIN_THREAD;
}

void BorderLine::OnNodeSet(Node* node) { Drawable::OnNodeSet(node); }
void BorderLine::OnSceneSet(Scene* scene) { Drawable::OnSceneSet(scene); }
void BorderLine::OnMarkedDirty(Node* node) { Drawable::OnMarkedDirty(node); }
void BorderLine::OnWorldBoundingBoxUpdate() { worldBoundingBox_.Merge(node_->GetWorldPosition()); }
void BorderLine::OnRemoveFromOctree() {}

bool BorderLine::DrawOcclusion(OcclusionBuffer* buffer) { return true; }
void BorderLine::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) { Drawable::DrawDebugGeometry(debug, depthTest); } // Draw bounding box
