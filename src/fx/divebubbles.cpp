/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../player/ship.h"

#include "divebubbles.h"

DiveBubbles::DiveBubbles(Context* context): LogicComponent(context),
    ship_{ nullptr },
    billboardSet_{ nullptr },
    bubbles_{},
    sinceBubble_{ 0.f }
{
}

void DiveBubbles::Start()
{
    if (!node_)
        return;

    billboardSet_ = node_->CreateComponent<BillboardSet>();
    billboardSet_->SetMaterial(RES(Material, "Materials/SmallBubble.xml"));
    billboardSet_->SetSorted(true);
    billboardSet_->SetViewMask(M_MAX_UNSIGNED - 1);
}

void DiveBubbles::Stop()
{
    bubbles_.Clear();
}

void DiveBubbles::Clear()
{
    billboardSet_->SetNumBillboards(0);
    billboardSet_->Commit();

    bubbles_.Clear();
}

void DiveBubbles::Update(float timeStep)
{
    for (Bubble& b: bubbles_)
    {
        b.age_ += timeStep;
        if (b.age_ > b.lifeTime_ || b.position_.y_ > -.1f)
            bubbles_.Remove(b);
    }

    if (ship_ && ship_->IsDiving())
        sinceBubble_ += timeStep;
    else
        sinceBubble_ = 0.f;

    AddBubbles();

    billboardSet_->SetNumBillboards(bubbles_.Size());

    for (unsigned b{ 0u }; b < bubbles_.Size(); ++b)
    {
        Bubble& bubble{ bubbles_.At(b) };
        bubble.velocity_ = bubble.velocity_.Lerp(1.7f * Vector3::UP, timeStep * 2.3f);
        bubble.position_ += bubble.velocity_ * timeStep;

        Billboard* billboard{ billboardSet_->GetBillboard(b) };
        billboard->enabled_ = true;
        billboard->rotation_ = 0.f;
        billboard->color_ = Color::WHITE.Transparent(1.f - 2.3f * bubble.size_);
        billboard->size_ = bubble.size_ * Vector2::ONE;
        billboard->position_ = bubble.position_;
    }

    billboardSet_->Commit();
}

void DiveBubbles::AddBubbles()
{
    if (!ship_ || !ship_->GetNode()->IsEnabled() || !ship_->IsDiving())
        return;

    const Node* shipGraphics{ ship_->GetNode()->GetChild("Graphics") };
    const Vector3 shipPos{ shipGraphics->GetWorldPosition() };
    const Vector3 shipDir{ shipGraphics->GetWorldDirection() };
    const Vector3 shipUp{ shipGraphics->GetWorldUp() };
    const Vector3 shipRight{ shipGraphics->GetWorldRight() };

    const float bubbleInterval{ .01f * (.5f + PowN(5.f * ship_->GetActionCharge(DIVE), 3))
                / Max(.5f, ship_->GetComponent<RigidBody>()->GetLinearVelocity().Length() * .1f) };

    while (sinceBubble_ > bubbleInterval)
    {
        sinceBubble_ -= bubbleInterval;
        Bubble bubble{};
        bubble.position_ = shipPos + (.31f * RandomSign() * shipRight + .37f * shipUp - .6f * shipDir);
        bubble.velocity_ -= shipDir;
        bubble.velocity_ *= 2.3f;
        const Vector3 offCenter{ Quaternion{ Random(360.f), bubble.velocity_ } * shipRight + RandomOffCenter(1.f) * bubble.velocity_ };
        bubble.position_ += offCenter * .11f;
        bubble.velocity_ += offCenter * .17f;

        if (bubble.position_.y_ < -.1f)
            bubbles_.Push(bubble);
    }
}
