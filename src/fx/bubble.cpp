/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"

#include "bubble.h"

StaticModelGroup* Bubble::bubbleGroup_{};

void Bubble::RegisterObject(Context *context)
{
    context->RegisterFactory<Bubble>();
}

Bubble::Bubble(Context* context): Effect(context),
    spinAxis_{ Vector3{ Random(), Random(), Random() }.Normalized() },
    spinVelocity_{ RandomSign() * Random(23.f, 42.f) },
    baseScale_{ Random(.25f, .95f) }
{
    SetUpdateEventMask(USE_UPDATE);
}

void Bubble::Start()
{
    Effect::Start();

    baseScale_ *= baseScale_;
    node_->SetName("Bubble");

    if (!bubbleGroup_)
    {
        bubbleGroup_ = MC->scene_->CreateComponent<StaticModelGroup>();
        bubbleGroup_->SetModel(RES(Model, "Models/Box.mdl"));
        bubbleGroup_->SetMaterial(RES(Material, "Materials/Bubble.xml"));
    }
}

void Bubble::Set(const Vector3& position)
{
    bubbleGroup_->AddInstanceNode(node_);

    Effect::Set(position);
}

void Bubble::Update(float timeStep)
{
    if (node_->GetPosition().y_ > 42.0f)
        Disable();

    node_->Translate(Vector3::UP * timeStep * (6.66f + MC->SinceLastReset() * 0.0023f), TS_WORLD);
    node_->Rotate(Quaternion{ timeStep * spinVelocity_, spinAxis_ });
    node_->SetWorldScale(Vector3{ MC->Sine(2.f  - baseScale_, baseScale_ * .88f, baseScale_ * 1.23f, spinVelocity_),
                                  MC->Sine(3.f  - baseScale_, baseScale_ * .88f, baseScale_ * 1.23f, spinVelocity_ + 2.f),
                                  MC->Sine(2.3f - baseScale_, baseScale_ * .88f, baseScale_ * 1.23f, spinVelocity_ + 3.f) });
}

void Bubble::Disable()
{
    bubbleGroup_->RemoveInstanceNode(node_);
    SceneObject::Disable();
}
