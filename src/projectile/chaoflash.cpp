/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../enemy/razor.h"
#include "../enemy/spire.h"
#include "../fx/soundeffect.h"
#include "../pickup/apple.h"
#include "../pickup/coin.h"
#include "../pickup/coinpump.h"
#include "../pickup/heart.h"
#include "../player/player.h"
#include "../player/ship.h"
#include "depthcharge.h"
#include "seeker.h"
#include "chaomine.h"
#include "brick.h"

#include "chaoflash.h"

void ChaoFlash::RegisterObject(Context* context)
{
    context->RegisterFactory<ChaoFlash>();
    context->MC->GetSample("Chaos");
}

ChaoFlash::ChaoFlash(Context* context): SceneObject(context),
    age_{ 0.f }
{
    SetUpdateEventMask(USE_UPDATE);
}

void ChaoFlash::Start()
{
    SceneObject::Start();

    node_->SetName("ChaoFlash");
    MC->arena_->AddToAffectors(node_);

    node_->SetScale(7.0f);
    chaoModel_ = node_->CreateComponent<StaticModel>();
    chaoModel_->SetModel(RES(Model, "Models/ChaoFlash.mdl"));
    chaoMaterial_ = RES(Material, "Materials/ChaoFlash.xml");
    chaoModel_->SetMaterial(chaoMaterial_);

    Node* sunNode{ MC->scene_->CreateChild("SunDisk") };
    sunNode->SetTransform(Vector3{ 0.f, 2.3f, -2.f }, Quaternion::IDENTITY, 42.f);
    StaticModel* sunPlane{ sunNode->CreateComponent<StaticModel>() };
    sunPlane->SetModel(RES(Model, "Models/Plane.mdl"));;
    sunMaterial_ = RES(Material, "Materials/SunDisc.xml");
    sunPlane->SetMaterial(sunMaterial_);

    node_->SetEnabled(false);

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetKinematic(true);
}

void ChaoFlash::Update(float timeStep)
{
    if (!IsEnabled()) return;

    age_ += timeStep;

    Color chaoColor{chaoMaterial_->GetShaderParameter("MatDiffColor").GetColor()};
    rigidBody_->SetMass(chaoColor.a_);
    const Color newDiffColor{ chaoColor.r_ * Clamp(Random(.1f , 1.23f), 0.f, 1.f),
                              chaoColor.g_ * Random(.23f, .9f),
                              chaoColor.b_ * Random(.16f, .5f),
                              chaoColor.a_ * Random(.42f, .9f)};
    chaoMaterial_->SetShaderParameter("MatDiffColor", chaoColor.Lerp(newDiffColor, Clamp(17.f * timeStep, 0.f, 1.f)));
    const Color newSpecColor{ Random( .3f, 1.5f),
                              Random( .5f, 1.8f),
                              Random( .4f, 1.4f),
                              Random(4.f, 64.f)};
    chaoMaterial_->SetShaderParameter("MatSpecColor", newSpecColor);
    node_->SetRotation(Quaternion(Random(360.f), Random(360.f), Random(360.f)));

    if (age_ > .05f)
        sunMaterial_->SetShaderParameter("MatDiffColor", Color(Random(1.f),
                                                               Random(1.f),
                                                               Random(1.f),
                                                               Max(.23f - Pow(Max(0.f, age_ - .13f), 2.f) * 5.f, 0.f)));
    if (age_ > .42f)
        Disable();
}

void ChaoFlash::Set(const Vector3 position, int colorSet)
{
    Player* owner{ MC->GetPlayerByColorSet(colorSet) };
    Vector<Ship*> ships{};
    unsigned points{ 0 };
    bool caughtApple{ false };
    bool caughtHeart{ false };

    age_ = 0.f;
    SceneObject::Set(position);

    SoundEffect* chaosSound{ SPAWN->Create<SoundEffect>() };
    chaosSound->Set(node_->GetWorldPosition());
    chaosSound->PlaySample(MC->GetSample("Chaos"), .666f);

    PODVector<RigidBody* > hitResults{};
    const float radius{ 7.f };
    chaoMaterial_->SetShaderParameter("MatDiffColor", Color(.1f, .5f, .2f, .5f));
    rigidBody_->SetMass(6.f);

    if (MC->PhysicsSphereCast(hitResults, node_->GetPosition(), radius, M_MAX_UNSIGNED))
    {
        for (RigidBody* hitResult : hitResults)
        {
            Node* hitNode{ hitResult->GetNode() };

            if (hitNode->GetName() == "PickupTrigger")
                hitNode = hitNode->GetParent();

            if (Ship* ship = hitNode->GetComponent<Ship>())
            {

                ships.Push(ship);
            }
            else if (Apple* apple = hitNode->GetComponent<Apple>())
            {
                caughtApple = true;
                apple->Respawn();
            }
            else if (Heart* heart = hitNode->GetComponent<Heart>())
            {
                caughtHeart = true;
                heart->Respawn();
            }
            else if (Seeker* seeker = hitNode->GetComponent<Seeker>())
            {

                ++points;
                Razor* razor{ SPAWN->Create<Razor>() };
                razor->Set(seeker->GetWorldPosition());
                razor->GetNode()->GetComponent<RigidBody>()->ApplyImpulse(
                            seeker->GetComponent<RigidBody>()->GetLinearVelocity() * 17.f);
                seeker->Disable();
            }
            else if (Brick* brick = hitNode->GetComponent<Brick>())
            {
                if (brick->GetWorldPosition().DistanceToPoint(GetWorldPosition()) < radius)
                {
                    ++points;
                    SPAWN->Create<Spire>()->Set(SPAWN->NearestGridPoint(brick->GetWorldPosition()));
                    brick->Disable();
                }
            }
            else if (Coin* coin = hitNode->GetComponent<Coin>())
            {
                coin->Disable();
                SPAWN->Create<CoinPump>()->Set(SPAWN->NearestGridPoint(coin->GetWorldPosition()));
            }
            else if (DepthCharge* depthCharge = hitNode->GetComponent<DepthCharge>())
            {
                depthCharge->SetCountdown(Random(10.f));
            }
            //Turn enemies into mines
            else
            {
                Enemy* e{ hitNode->GetDerivedComponent<Enemy>() };
                if (e && !e->IsInstanceOf<ChaoMine>())
                {
                    ChaoMine* chaoMine{ SPAWN->Create<ChaoMine>() };
                    chaoMine->Set(e->GetWorldPosition(), colorSet);
                    points += 2 + Random(3) * e->GetWorth();
                    e->Disable();
                }
            }
        }

        owner->AddScore(points);

        if (points == 0 && !caughtApple && !caughtHeart)
            SPAWN->SpawnDeathFlower(GetWorldPosition());
    }

    //Hand out upgrades
    for (Ship* s: ships)
    {
        if (caughtApple)
            s->PowerupWeapons();
        if (caughtHeart)
            s->PowerupShield();
    }

    //Swap ship positions
    if (ships.Size() > 1)
    {
        const Vector3 firstPos{ ships[0]->GetWorldPosition() };

        for (unsigned s{0}; s < ships.Size(); ++s)
        {
            if (s == ships.Size() - 1)
                ships[s]->GetNode()->SetPosition(firstPos);
            else
                ships[s]->GetNode()->SetPosition(ships[s + 1]->GetWorldPosition());
        }
    }
    else if (ships.Size())
    {
        ships[0]->GetNode()->SetPosition(Quaternion{ Random(360.f), Vector3::UP } * (Vector3::FORWARD * Random(5.f)) +
                                       node_->GetPosition() * Vector3{ 1.f, 0.f, 1.f });
    }
}

void ChaoFlash::Disable()
{
    SceneObject::Disable();
}
