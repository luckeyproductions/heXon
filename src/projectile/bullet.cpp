/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../spawnmaster.h"
#include "../enemy/razor.h"
#include "../enemy/spire.h"
#include "../environment/arena.h"
#include "../fx/hitfx.h"

#include "bullet.h"

HashMap<int, StaticModelGroup*> Bullet::bulletGroups_{};

void Bullet::RegisterObject(Context *context)
{
    context->RegisterFactory<Bullet>();
}

Bullet::Bullet(Context* context): SceneObject(context),
    colorSet_{},
    age_{ 0.f },
    timeSinceHit_{ 0.f },
    lifeTime_{ 1.f },
    fading_{ false },
    damage_{ 0.f }
{
    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void Bullet::Start()
{
    SceneObject::Start();

    node_->SetName("Bullet");
    node_->AddTag("Bullet");

    node_->SetEnabled(false);
    node_->SetScale(Vector3{ 1.f + damage_, 1.f + damage_, .1f });

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(.5f);
    rigidBody_->SetLinearFactor(Vector3::ONE - Vector3::UP);
    rigidBody_->SetFriction(0.f);

    Light* light{ node_->CreateComponent<Light>() };
    light->SetBrightness(4.2f);
    light->SetRange(5.f);

    if (bulletGroups_.IsEmpty())
    {
        for (unsigned c{ 0 }; c < MC->colorSets_.Size(); ++c)
        {
            bulletGroups_[c] = MC->scene_->CreateComponent<StaticModelGroup>();
            bulletGroups_[c]->SetModel(RES(Model, "Models/Bullet.mdl"));
            bulletGroups_[c]->SetMaterial(MC->colorSets_[c].bulletMaterial_);
        }
    }
}

void Bullet::Update(float timeStep)
{
    age_ += timeStep;
    node_->SetScale(Vector3{ Max(1.75f - 10.f * age_, 1.f + damage_),
                             Max(1.75f - 10.f * age_, 1.f + damage_),
                             Min(Min(35.f * age_, 2.f), Max(2.f - timeSinceHit_ * 42.f, .1f)) });
    if (age_ > lifeTime_)
        Disable();
}

void Bullet::FixedUpdate(float timeStep)
{
    if (!fading_)
        HitCheck(timeStep);
}

void Bullet::Set(const Vector3& position, const int colorSet, const Vector3& direction, const Vector3& force, float damage)
{
    age_ = 0.f;
    timeSinceHit_ = 0.f;
    fading_ = false;
    colorSet_ = colorSet;
    damage_ = damage;

    Light* light{ node_->GetComponent<Light>() };

    if (light->IsEnabled())
    {
        light->SetColor( MC->colorSets_[colorSet].colors_.first_ * (1.f + damage_) );
    }

    rigidBody_->SetLinearVelocity(Vector3::ZERO);
    rigidBody_->ResetForces();
    SceneObject::Set(position);
    rigidBody_->ApplyImpulse(force);
    node_->LookAt(node_->GetPosition() + direction);

    bulletGroups_[colorSet]->AddInstanceNode(node_);
}

void Bullet::Disable()
{
    bulletGroups_[colorSet_]->RemoveInstanceNode(node_);

    fading_ = true;
    SceneObject::Disable();
    UnsubscribeFromEvent(E_SCENEUPDATE);

}

void Bullet::HitCheck(float timeStep)
{
    if (!fading_)
    {
        PODVector<PhysicsRaycastResult> hitResults{};
        const Ray bulletRay{ node_->GetPosition() - rigidBody_->GetLinearVelocity() * timeStep, node_->GetDirection() };
        const float rayLength{ 2.3f * rigidBody_->GetLinearVelocity().Length() * timeStep };

        if (MC->PhysicsRayCast(hitResults, bulletRay, rayLength, M_MAX_UNSIGNED))
        {
            for (PhysicsRaycastResult result: hitResults)
            {
                Node* node{ result.body_->GetNode() };
                Enemy* enemy{ node->GetDerivedComponent<Enemy>() };

                if (!enemy && node->HasTag("Enemy"))
                {
                    enemy = node->GetParentDerivedComponent<Enemy>();
                    node = enemy->GetNode();
                    result.body_ = node->GetComponent<RigidBody>();
                }

                if (!result.body_->IsTrigger() && result.body_->GetCollisionLayer() != 5)
                {
                    //Add effect
                    result.body_->ApplyImpulse(rigidBody_->GetLinearVelocity() * .05f);
                    HitFX* hitFx{ SPAWN->Create<HitFX>() };
                    hitFx->Set(result.position_, colorSet_, true);

                    //Deal damage
                    if (enemy)
                        enemy->Hit(damage_, colorSet_);

                    Disable();
                }
            }
        }
    }
}
