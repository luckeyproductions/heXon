/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../fx/hitfx.h"
#include "../fx/soundeffect.h"
#include "../fx/trail.h"
#include "../environment/arena.h"
#include "../player/player.h"
#include "../player/ship.h"
#include "chaomine.h"

#include "seeker.h"

void Seeker::RegisterObject(Context *context)
{
    context->RegisterFactory<Seeker>();
    context->MC->GetSample("Seeker");
}

Seeker::Seeker(Context* context): SceneObject(context),
    rigidBody_{ nullptr },
    trails_{ nullptr, nullptr },
    age_{ 0.f },
    lifeTime_{ 4.2f },
    damage_{ 1.23f },
    super_{ false }
{
    big_ = false;

    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void Seeker::Start()
{
    SceneObject::Start();

    node_->SetName("Seeker");
//    MC->arena_->AddToAffectors(node_);

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(1.23f);
    rigidBody_->SetLinearDamping(.42f);
    rigidBody_->SetTrigger(true);
    rigidBody_->SetLinearFactor(Vector3::ONE - Vector3::UP);

    CollisionShape* trigger{ node_->CreateComponent<CollisionShape>() };
    trigger->SetSphere(.42f);

    ParticleEmitter* particleEmitter{ node_->CreateComponent<ParticleEmitter>() };
    particleEmitter->SetEffect(RES(ParticleEffect, "Particles/Seeker.xml"));

    Light* light{ node_->CreateComponent<Light>() };
    light->SetRange(5.f);
    light->SetBrightness(1.3f);
    light->SetColor(Color::WHITE);
}

void Seeker::Update(float timeStep)
{
    age_ += timeStep;

    if (age_ > lifeTime_)
    {
        HitFX* hitFx{ SPAWN->Create<HitFX>() };
        hitFx->Set(GetWorldPosition(), 0, false);
        Disable();
    }

    node_->LookAt(node_->GetWorldPosition() + rigidBody_->GetLinearVelocity().Normalized());

    for (Trail* tg: { trails_.first_, trails_.second_ } )
    {
        if (tg)
        {
            const float speed{ rigidBody_->GetLinearVelocity().Length() };
            tg->SetTailLength(speed * .027f + .055f);
            tg->SetWidthScale(MC->Sine(speed * 2.3f, .42f, .666f / (1.f + speed), Random(.05f)));
        }
    }
}

void Seeker::FixedUpdate(float timeStep)
{
//    const Vector3 fieldVector{ GetSubsystem<Arena>()->ForceFieldAt(node_->GetWorldPosition()) };
//    const Vector3 forceVector{ fieldVector.ProjectOntoPlane(Vector3::UP) };

    rigidBody_->ApplyForce(timeStep * (/*-forceVector.Normalized() * Min(1.f, fieldVector.y_ * Max(0.f, forceVector.DotProduct(rigidBody_->GetLinearVelocity()))) +*/
                                       (TargetPosition() - node_->GetPosition() - rigidBody_->GetLinearVelocity() * .17f).Normalized())
                           * (420.f + 666.f * super_));
}

void Seeker::HandleTriggerStart(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!node_->IsEnabled())
        return;

    PODVector<RigidBody*> collidingBodies{};
    rigidBody_->GetCollidingBodies(collidingBodies);

    for (unsigned i{ 0 }; i < collidingBodies.Size(); ++i)
    {
        RigidBody* collider{ collidingBodies[i] };

        if (collider->GetNode()->HasComponent<Ship>())
        {
            Ship* hitShip{ collider->GetNode()->GetComponent<Ship>() };
            if (!hitShip->IsDiving())
            {
                hitShip->Hit(damage_, false);

                SPAWN->Create<HitFX>()
                        ->Set(node_->GetPosition(), 0, false);
                collider->ApplyImpulse(rigidBody_->GetLinearVelocity() * .5f);
                Disable();
            }
        }
        else if (collider->GetNode()->HasComponent<ChaoMine>())
        {
            collider->GetNode()->GetComponent<ChaoMine>()->Hit(damage_, 0);
            Disable();
        }
        else if (collider->GetNode()->HasComponent<Seeker>())
        {

            SPAWN->Create<HitFX>()
                    ->Set(node_->GetPosition(), 0, false);

            Disable();
        }
    }
}

Vector3 Seeker::TargetPosition()
{
    Player* nearestPlayer{ MC->GetNearestPlayer(GetWorldPosition()) };

    if (nearestPlayer)
        return nearestPlayer->GetWorldPosition();
    else
        return Vector3::ZERO;
}

void Seeker::Set(const Vector3& position, bool sound, bool super)
{
    super_ = super;
    age_ = 0.f;

    SceneObject::Set(position);

    node_->SetScale(1.f + .2f * super);
    rigidBody_->ResetForces();
    rigidBody_->SetLinearVelocity(Vector3::ZERO);
    AddTrails();

    if (sound)
    {
        SoundEffect* seekerSound{ SPAWN->Create<SoundEffect>() };
        seekerSound->Set(node_->GetWorldPosition());
        seekerSound->PlaySample(MC->GetSample("Seeker"), .666f);
    }

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Seeker, HandleTriggerStart));

    rigidBody_->ApplyImpulse((TargetPosition() - node_->GetPosition()).Normalized() * 2.3f);
}

void Seeker::Disable()
{
    RemoveTrails();
    SceneObject::Disable();
}

void Seeker::SetLinearVelocity(const Vector3& velocity)
{
    rigidBody_->SetLinearVelocity(velocity);
}

void Seeker::AddTrails()
{
    return; /// Trails seem to cause crash
    RemoveTrails();

    for (bool first: { true, false })
    {
        Trail* tg{ node_->CreateComponent<Trail>() };
        tg->SetMatchNodeOrientation(true);
        tg->SetDrawHorizontal(true);
        tg->SetDrawMirrored(first);
        tg->SetDrawVertical(false);
        tg->SetNumTails(9);
        tg->SetColorForHead({ .666f, .42f, 1.f,  1.f });
        tg->SetColorForTip({ 0.f,    .1f,   .23f, .42f });

        if (first)
            trails_.first_ = tg;
        else
            trails_.second_ = tg;
    }
}

void Seeker::RemoveTrails()
{
    if (trails_.first_)
    {
        trails_.first_->Remove();
        trails_.first_ = nullptr;
    }

    if (trails_.second_)
    {
        trails_.second_->Remove();
        trails_.second_ = nullptr;
    }
}
