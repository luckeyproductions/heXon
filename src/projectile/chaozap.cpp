/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../spawnmaster.h"
#include "../enemy/baphomech.h"
#include "../pickup/coin.h"
#include "../player/player.h"
#include "brick.h"
#include "chaomine.h"
#include "depthcharge.h"
#include "seeker.h"

#include "chaozap.h"

void ChaoZap::RegisterObject(Context* context)
{
    context->RegisterFactory<ChaoZap>();

    for (int i{ 1 }; i < 6; ++i)
        context->MC->GetSample("Mine" + String{ i });
}

ChaoZap::ChaoZap(Context* context): SceneObject(context),
    size_{ 5.f },
    age_{ 0.f }
{
    SetUpdateEventMask(USE_UPDATE);
}

void ChaoZap::Start()
{
    SceneObject::Start();

    blink_ = false;

    node_->SetName("ChaoZap");
    node_->SetEnabled(false);

    chaoModel_ = node_->CreateComponent<StaticModel>();
    chaoMaterial_ = RES(Material, "Materials/ChaoFlash.xml")->Clone();
    chaoModel_->SetModel(RES(Model, "Models/ChaoFlash.mdl"));
    chaoModel_->SetMaterial(chaoMaterial_);

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetKinematic(true);
}

void ChaoZap::Update(float timeStep)
{
    if (!IsEnabled())
        return;

    age_ += timeStep;

    Color chaoColor{ chaoMaterial_->GetShaderParameter("MatDiffColor").GetColor() };
    rigidBody_->SetMass(chaoColor.a_ * 5.f);

    const Color newDiffColor{ chaoColor.r_ * Clamp(Random(.25f , 1.23f), 0.f, 1.f),
                              chaoColor.g_ * Random(.23f, .9f),
                              chaoColor.b_ * Random(.16f, .5f),
                              chaoColor.a_ * Random(.42f, .9f)};
    chaoMaterial_->SetShaderParameter("MatDiffColor", chaoColor.Lerp(newDiffColor, Clamp(23.f * timeStep, 0.f, 1.f)));

    const Color newSpecColor{ Random( .3f, 1.5f),
                              Random( .5f, 1.8f),
                              Random( .4f, 1.4f),
                              Random(4.f, 64.f) };
    chaoMaterial_->SetShaderParameter("MatSpecColor", newSpecColor);

    node_->SetRotation(Quaternion(Random(360.f), Random(360.f), Random(360.f)));

    if (age_ > .23f)
        Disable();
}

void ChaoZap::Set(const Vector3& position, int colorSet)
{
    age_ = 0.f;
    SceneObject::Set(position);

    node_->SetScale(size_);
    chaoMaterial_->SetShaderParameter("MatDiffColor", Color(.1f, .5f, .2f, .5f));
    rigidBody_->SetMass(size_ * .5f);
    PlaySample(MC->GetSample("Mine" + String(Random(5) + 1)), .75f);

    PODVector<RigidBody*> hitResults{};

    if (MC->PhysicsSphereCast(hitResults,node_->GetPosition(), size_, M_MAX_UNSIGNED))
    {
        for (RigidBody* r: hitResults)
        {
            Node* hitNode{ r->GetNode() };

            if (Seeker* seeker = hitNode->GetComponent<Seeker>())
            {
                MC->GetPlayerByColorSet(colorSet)->AddScore(Random(2, 3));
                seeker->Disable();
            }
            else if (Brick* brick = hitNode->GetComponent<Brick>())
            {
                MC->GetPlayerByColorSet(colorSet)->AddScore(Random(5, 7));
                brick->Disable();
            }
            else if (Coin* coin = hitNode->GetComponent<Coin>())
            {
                coin->Disable();
            }
            else if (DepthCharge* depthCharge = hitNode->GetComponent<DepthCharge>())
            {
                ChaoMine* chaoMine{ SPAWN->Create<ChaoMine>() };
                chaoMine->Set(depthCharge->GetWorldPosition(), depthCharge->GetColorSet());
                depthCharge->Disable();
            }
            else
            {
                Enemy* e{ hitNode->GetDerivedComponent<Enemy>() };

                if (e)
                {
                    if (e->IsInstanceOf<Baphomech>())
                    {
                        e->Hit(23.f, colorSet);
                    }
                    else if (!e->IsInstanceOf<ChaoMine>())
                    {
                        MC->GetPlayerByColorSet(colorSet)->AddScore(Random(2 * e->GetWorth(), 3 * e->GetWorth()));
                        e->Hit(e->GetHealth(), colorSet);
                    }
                }
            }
        }
    }
}
