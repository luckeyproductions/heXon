/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../spawnmaster.h"
#include "../fx/explosion.h"
#include "../enemy/enemy.h"

#include "depthcharge.h"

DepthCharge::DepthCharge(Context* context): SceneObject(context),
    model_{ nullptr },
    rigidBody_{ nullptr },
    collider_{ nullptr },
    countdown_{ 0.f },
    colorSet_{}
{
    big_ = false;

    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void DepthCharge::Start()
{
    SceneObject::Start();

    graphicsNode_ = node_->CreateChild("Graphics");
    model_ = graphicsNode_->CreateComponent<StaticModel>();
    model_->SetModel(RES(Model, "Models/DepthCharge.mdl"));

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetMass(.5f);
    rigidBody_->SetLinearFactor(Vector3::ONE - Vector3::UP);
    rigidBody_->SetAngularFactor(Vector3::ZERO);

    collider_ = node_->CreateComponent<CollisionShape>();
    collider_->SetSphere(1.5f);
}

void DepthCharge::Set(const Vector3& position, const Vector3& direction, int colorSet)
{
    countdown_ = 11.f;
    SceneObject::Set(position + direction * 1.5f);

    colorSet_ = colorSet;
    model_->SetMaterial(0, MC->colorSets_[colorSet_].hullMaterial_);
    model_->SetMaterial(1, MC->colorSets_[colorSet_].glowMaterial_);
    model_->SetMaterial(2, RES(Material, "Materials/Digit.xml")->Clone());

    ValueAnimation* posAnim{ new ValueAnimation{ context_ } };
    posAnim->SetKeyFrame(0.f, -direction * 1.5f);
    posAnim->SetKeyFrame(1.7f, Vector3::ZERO);
    posAnim->SetInterpolationMethod(IM_SINUSOIDAL);

    const float rotTime{ 1.1f };
    ValueAnimation* rotAnim{ new ValueAnimation{ context_ } };
    rotAnim->SetKeyFrame(0.f, Quaternion{ -90.f, direction.CrossProduct(Vector3::UP) });
    rotAnim->SetKeyFrame(rotTime, Quaternion{ rotTime * 72.f, Vector3::UP });
    rotAnim->SetInterpolationMethod(IM_LINEAR);

    ValueAnimation* scaleAnim{ new ValueAnimation{ context_ } };
    scaleAnim->SetKeyFrame(0.f,  Vector3::ONE * 2/3.f);
    scaleAnim->SetKeyFrame(.42f, Vector3::ONE);
    scaleAnim->SetInterpolationMethod(IM_SINUSOIDAL);

    ObjectAnimation* launchAnim{ new ObjectAnimation{ context_ } };
    launchAnim->AddAttributeAnimation("Position", posAnim, WM_ONCE);
    launchAnim->AddAttributeAnimation("Rotation", rotAnim, WM_ONCE);
    launchAnim->AddAttributeAnimation("Scale", scaleAnim, WM_ONCE);

    graphicsNode_->SetObjectAnimation(launchAnim);
    rigidBody_->ApplyImpulse(direction * 2.3f);

    SubscribeToEvent(node_, E_NODECOLLISION, DRY_HANDLER(DepthCharge, HandleNodeCollision));
}

void DepthCharge::Detonate()
{
    Explosion* explosion{ SPAWN->Create<Explosion>() };
    explosion->Set(node_->GetPosition(),
                   MC->colorSets_[colorSet_].colors_.first_,
                   1.f, colorSet_);
    explosion->SetBlastForce(1024.f);

    PODVector<RigidBody* > hitResults{};
    if (MC->PhysicsSphereCast(hitResults, node_->GetPosition(), 3.f, M_MAX_UNSIGNED))
    {
        for (RigidBody* hitResult: hitResults)
        {
            Node* hitNode{ hitResult->GetNode() };
            Enemy* e{ hitNode->GetDerivedComponent<Enemy>() };
            if (e)
            {
                const float distance{ node_->GetWorldPosition().DistanceToPoint(hitNode->GetWorldPosition()) };
                e->Hit(7.f / (Cbrt(distance) + 1.f), colorSet_);
            }
        }
    }

    Disable();
}

void DepthCharge::Update(float timeStep)
{
    graphicsNode_->Yaw(timeStep * 72.f);

    countdown_ -= timeStep * 7/4.f;
    if (countdown_ < 0.f)
    {
        Detonate();
        return;
    }

    if (countdown_ < 10.f)
    {
        Material* mat{ model_->GetMaterial(2) };
        mat->SetUVTransform(Vector2::DOWN * countdown_ * .1f, 0.f, 1.f);
    }
}


void DepthCharge::HandleNodeCollision(StringHash /*eventType*/, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollision::P_OTHERNODE].GetPtr()) };
    if (!otherNode)
        return;

    Enemy* e{ otherNode->GetDerivedComponent<Enemy>() };
    if (e)
        Detonate();
}


void DepthCharge::FixedUpdate(float timeStep)
{
    const Vector3 v{ rigidBody_->GetLinearVelocity() };
    rigidBody_->ApplyForce(-5.5f * v.LengthSquared() * v.Normalized() * timeStep);
}
