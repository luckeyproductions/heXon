/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SEEKER_H
#define SEEKER_H

#include "../sceneobject.h"

class Trail;

class Seeker: public SceneObject
{
    DRY_OBJECT(Seeker, SceneObject);

    friend class ChaoFlash;
    friend class ChaoZap;

public:
    static void RegisterObject(Context* context);
    Seeker(Context* context);

    void Start() override;
    void Set(const Vector3& position, bool sound = true, bool super = false);
    void Disable() override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void SetLinearVelocity(const Vector3& velocity);

    void HandleTriggerStart(StringHash eventType, VariantMap &eventData);

private:
    void AddTrails();
    void RemoveTrails();
    Vector3 TargetPosition();

    RigidBody* rigidBody_;
    Pair<Trail*, Trail*> trails_;

    float age_;
    float lifeTime_;
    float damage_;
    bool super_;
};

#endif // SEEKER_H
