/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "settings.h"
#include "effectmaster.h"

#include "hexocam.h"

void heXoCam::RegisterObject(Context* context)
{
    context->RegisterFactory<heXoCam>();
}

heXoCam::heXoCam(Context* context): LogicComponent(context)
{
}

void heXoCam::Start()
{
    GetSubsystem<Audio>()->SetListener(node_->CreateComponent<SoundListener>());

    /* Ready for VR :)
    Node* leftEye{ node_->CreateChild("Left Eye") };
    leftEye->SetPosition(Vector3::LEFT);
    stereoCam_.first_ = leftEye->CreateComponent<Camera>();
    Node* rightEye{ node_->CreateChild("Right Eye") };
    rightEye->SetPosition(Vector3::RIGHT);
    stereoCam_.second_ = rightEye->CreateComponent<Camera>();
    */

    camera_ = node_->CreateComponent<Camera>();
    camera_->SetFarClip(128.f);
    node_->SetPosition({ 0.f, 42.f, -23.f });
    node_->SetRotation({ 65.f, 0.f, 0.f });

    SetupViewport();

    SubscribeToEvent(E_ENTERLOBBY, DRY_HANDLER(heXoCam, EnterLobby));
    SubscribeToEvent(E_ENTERPLAY,  DRY_HANDLER(heXoCam, EnterPlay));
}

void heXoCam::SetupViewport()
{
    Settings* settings{ GetSubsystem<Settings>() };
    SharedPtr<Viewport> viewport{ new Viewport{ context_, MC->scene_, camera_ } };
    viewport_ = viewport;

    //Add anti-asliasing, bloom and a greyscale effects
//    XMLFile* file{ new XMLFile(context_) };
//    file->LoadFile("RenderPaths/Deferred.xml");
//    viewport_->SetRenderPath(file);
    effectRenderPath_ = GetSubsystem<Renderer>()->GetDefaultRenderPath();
//    effectRenderPath_->Load(RES(XMLFile, "RenderPaths/ForwardPixel.xml"));
    effectRenderPath_->Append(RES(XMLFile, "PostProcess/FXAA3.xml"));
    effectRenderPath_->SetEnabled("FXAA3", settings->GetAntiAliasing());
    effectRenderPath_->Append(RES(XMLFile, "PostProcess/Tonemap.xml"));
    effectRenderPath_->Append(RES(XMLFile, "PostProcess/BloomHDR.xml"));
    effectRenderPath_->SetShaderParameter("BloomHDRThreshold", 5.f);
    effectRenderPath_->SetShaderParameter("BloomHDRMix", Vector2{ 1.f, .42f });
    effectRenderPath_->SetShaderParameter("BloomHDRBlurRadius", 64.f);
    effectRenderPath_->SetShaderParameter("BloomHDRBlurSigma", 1.f);
    effectRenderPath_->SetEnabled("BloomHDR", settings->GetBloom());

    effectRenderPath_->Append(RES(XMLFile, "PostProcess/GreyScale.xml"));
    SetGreyScale(false);

    viewport_->SetRenderPath(effectRenderPath_);
    GetSubsystem<Renderer>()->SetViewport(0u, viewport_);

    effectRenderPath_->SetEnabled("TonemapReinhardEq3", false);
    effectRenderPath_->SetEnabled("TonemapReinhardEq4", true);
    settings->SetSetting("Bloom", settings->GetBloom());
}

void heXoCam::Update(float timeStep)
{
    node_->SetPosition(node_->GetPosition().Lerp(closeUp_ ? Vector3{ 0.f, 13.f,  -6.55f }
                                                          : Vector3{ 0.f, 43.f, -24.f },
                                                 Clamp(5.f * timeStep, 0.f, 1.f)));
}

void heXoCam::SetGreyScale(const bool enabled)
{
    effectRenderPath_->SetEnabled("GreyScale", enabled);
}

void heXoCam::EnterLobby(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    closeUp_ = true;
    effectRenderPath_->SetShaderParameter("BloomHDRThreshold", .42f);
//    GetSubsystem<EffectMaster>()->TranslateTo(node_, Vector3(0.0f, 43.0f, -24.0f), 2.3f);
}

void heXoCam::EnterPlay(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    closeUp_ = false;
    effectRenderPath_->SetShaderParameter("BloomHDRThreshold", .28f);
//    GetSubsystem<EffectMaster>()->TranslateTo(node_, Vector3(0.0f, 43.0f, -24.0f), 2.3f);

}
