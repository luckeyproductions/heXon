/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H


#include "luckey.h"

namespace Dry {
class Node;
class Scene;
}

class SceneObject: public LogicComponent
{
    DRY_OBJECT(SceneObject, LogicComponent);

public:
    SceneObject(Context* context);

    virtual void Set(const Vector3& position);
    virtual void Set(const Vector3& position, const Quaternion& rotation);
    virtual void Disable();

    Vector3 GetWorldPosition() const { return node_->GetWorldPosition(); }
    bool IsEmerged() const { return GetWorldPosition().y_ > 0.f; }
    bool IsEnabled() const { return node_->IsEnabled(); }

    void PlaySample(Sound* sample, const float gain = .5f, bool localized = true);
    void StopAllSound();

protected:
    void AddSampleSource();
    bool IsPlayingSound();
    void Emerge(const float timeStep);
    virtual void Blink(const Vector3& newPosition, const Vector3& flashOffset = Vector3::ZERO);

    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

    SharedPtr<Node> graphicsNode_;
    Vector<SoundSource3D*> sampleSources3D_;
    bool blink_;
    bool big_;
};

#endif // SCENEOBJECT_H
