/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include "luckey.h"

#define SETTINGSPATH MC->GetStoragePath() + "/Settings.xml"

using ToggleFloat = Pair<bool, float>;

class Settings: public Object
{
    DRY_OBJECT(Settings, Object);

public:
    Settings(Context* context);
    
    bool Load();
    void Save();

    void SetSetting(const String& name, const Variant& value);

    IntVector2 GetResolution() const { return { width_, height_ }; }
    bool GetFullscreen() const noexcept { return fullscreen_; }
    bool GetAntiAliasing() const noexcept { return antiAliasing_; }
    bool GetManyLights() const noexcept { return manyLights_; }
    bool GetBloom() const noexcept { return bloom_; }
    bool GetVSync() const noexcept { return vSync_; }

    bool GetEffectsOn() const noexcept { return effects_.first_; }
    float GetEffectsGain() const noexcept { return effects_.second_; }
    bool GetMusicOn() const noexcept { return music_.first_; }
    float GetMusicGain() const noexcept { return music_.second_; }

private:
    Scene* settingsScene_;
    Camera* settingsCam_;
    Vector<Node*> panels_;

    int width_;
    int height_;
    int refreshRate_;
    bool fullscreen_;
    bool antiAliasing_;
    bool manyLights_;
    bool bloom_;
    bool vSync_;

    ToggleFloat effects_;
    ToggleFloat music_;
};

#endif // SETTINGS_H
