/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MENUMASTER_H
#define MENUMASTER_H

#include "../mastercontrol.h"
#include "settingspage.h"

#define ARMPOS_X (visible_ ? (armature_->GetRoot()->GetWidth() - armature_->GetWidth() + armature_->GetBorder().Left()) * 3 / 4 : armature_->GetRoot()->GetWidth())

class MenuMaster: public Object
{
    DRY_OBJECT(MenuMaster, Object);

public:
    MenuMaster(Context* context);

    static CheckBox* CreateCheckbox(UIElement* parent);

    void SetVisible(bool visible)
    {
        if (visible_ != visible)
        {
            if (!visible_)
                Show();
            else
                Hide();
        }
        else
        {
            return;
        }
    }

    bool IsVisible() { return visible_; }

    void NextPage();
    void PreviousPage();
    void SetPage(unsigned page);

    void NextElement();
    void PreviousElement();

    void Increment(float amount);
    void Confirm();

private:
    void CreateGraphicsPage();
    void CreateAudioPage();
    void CreateInputPage();
    void CreatePageArrows(const IntVector2 &screenSize);
    void CreatePageButtons();

    void Show();
    void Hide();
    void TurnPage();
    void HandleMenuAction(StringHash eventType, VariantMap &eventData);
    void HandleEnterLobby(StringHash eventType, VariantMap &eventData);
    void HandleScreenMode(StringHash eventType, VariantMap& eventData);
    void HandlePageButtonPushed(StringHash eventType, VariantMap &eventData);

    const Vector<String> pageNames_{ "Graphics", "Audio", "Input" };

    UIElement* menuRoot_;
    BorderImage* shade_;
    BorderImage* armature_;
    Text* title_;
    SettingsPage* graphicsPage_;
    SettingsPage* audioPage_;
    SettingsPage* inputPage_;
    Vector<SettingsPage*> pages_;
    Vector<Button*> pageButtons_;

    bool visible_;
    unsigned lastToggled_;
    unsigned lastStepped_;
    IntVector2 lastStep_;
    unsigned currentPage_;
};

#endif // MENUMASTER_H
