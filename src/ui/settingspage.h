/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SETTINGSPAGE_H
#define SETTINGSPAGE_H

#include "../luckey.h"

struct Setting
{
//    Setting(const String& name, const Variant& value):
//        name_{ name },
//        value_{ value }
//    {}

    StringHash ToHash() { return StringHash(name_); }

    String name_;
    Variant value_;
};

class SettingsPage: public Window
{
    DRY_OBJECT(SettingsPage, Window);

public:
    SettingsPage(Context* context);

    void AddSettings(const Vector<Setting>& settings);
    PODVector<UIElement*> GetFocusableElements() const;

private:
    void CreateSetting(const Setting& setting);

    void HandleResolutionButtonPressed(StringHash eventType, VariantMap& eventData);
    void HandleValueChanged(StringHash eventType, VariantMap& eventData);
    void HandleSliderFocusChanged(StringHash eventType, VariantMap& eventData);

    Vector<Setting> settings_;
    Text* currentResolutionText_;
};

#endif // SETTINGSPAGE_H
