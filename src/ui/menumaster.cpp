/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "menumaster.h"

#include "../settings.h"
#include "../inputmaster.h"
#include "../environment/highest.h"

MenuMaster::MenuMaster(Context* context): Object(context),
    menuRoot_{ nullptr },
    shade_{ nullptr },
    armature_{ nullptr },
    title_{ nullptr },
    graphicsPage_{ nullptr },
    audioPage_{ nullptr },
    inputPage_{ nullptr },
    pages_{},
    pageButtons_(),
    visible_{ false },
    lastToggled_{ 0u },
    lastStepped_{ 0u },
    lastStep_{ IntVector2::ZERO },
    currentPage_{ 0u }
{
    UI* ui{ GetSubsystem<UI>() };
    const IntVector2 screenSize{ 1920, 1080/*GRAPHICS->GetSize()*/ };

    menuRoot_ = ui->GetRoot()->CreateChild<UIElement>();
    shade_ = menuRoot_->CreateChild<BorderImage>();
    shade_->SetSize(screenSize);
    shade_->SetTexture(RES(Texture2D, "UI/Shade.png"));
    shade_->SetFullImageRect();
    shade_->SetBlendMode(BLEND_ALPHA);
    shade_->SetOpacity(0.f);

    armature_ = menuRoot_->CreateChild<BorderImage>();
    armature_->SetSize(screenSize);
    armature_->SetPosition(screenSize.x_, -screenSize.y_ / 17);
    armature_->SetTexture(RES(Texture2D, "UI/Menu.png"));
    armature_->SetImageBorder({ 1, 1, 1, 1 });
    const int borderWidth{ Max(0, (screenSize.x_ - screenSize.y_) / 2) };
    armature_->SetBorder({ borderWidth, 0, borderWidth, 0 });
    armature_->SetBlendMode(BLEND_ALPHA);

//    const int borderWidth{ (Max(screenSize.x_, screenSize.y_) - Min(screenSize.x_, screenSize.y_)) / 2 };
//    if (screenSize.x_ > screenSize.y_)
//        armature_->SetBorder({ borderWidth, 0, borderWidth, 0 });
//    else
//        armature_->SetBorder({ 0, borderWidth, 0, borderWidth });

    title_ = armature_->CreateChild<Text>();
    title_->SetFont(FONT_RABBIT);
    title_->SetFontSize(55);
    title_->SetColor(Color::AZURE.Lerp(Color::CYAN, .23f));
    title_->SetAlignment(HA_CENTER, VA_TOP);
    title_->SetPosition(0, 192);
    title_->SetTextEffect(TE_STROKE);
    title_->SetEffectColor(Color::CYAN.Transparent(.2f));
    title_->SetOpacity(.7f);
    title_->SetText(pageNames_.Front());

    CreatePageButtons();
    CreatePageArrows(screenSize);
    CreateGraphicsPage();
    CreateAudioPage();
    CreateInputPage();

    pages_.Push(graphicsPage_);
    pages_.Push(audioPage_);
    pages_.Push(inputPage_);

    SubscribeToEvent(E_MENUACTION, DRY_HANDLER(MenuMaster, HandleMenuAction));
    SubscribeToEvent(E_ENTERLOBBY, DRY_HANDLER(MenuMaster, HandleEnterLobby));
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(MenuMaster, HandleScreenMode));
}

CheckBox* MenuMaster::CreateCheckbox(UIElement* parent)
{
    ResourceCache* cache{ parent->GetSubsystem<ResourceCache>() };
    CheckBox* box{ parent->CreateChild<CheckBox>() };
    box->SetBlendMode(BLEND_ALPHA);
    box->SetAlignment(HA_CENTER, VA_CENTER);
    box->SetTexture(cache->GetResource<Texture2D>("UI/CheckBox.png"));
    box->SetImageRect({ 0, 0, 128, 128 });
    box->SetHoverOffset(0, 128);
    box->SetCheckedOffset(128, 0);
    box->SetSize(96, 96);
    box->SetPosition(224, 0);

    return box;
}

void MenuMaster::CreateGraphicsPage()
{
    Settings* sett{ GetSubsystem<Settings>() };
    const Vector<Setting> graphicsSettings_{
        { "Bloom",          sett->GetBloom() },
        { "Anti-Aliasing",  sett->GetAntiAliasing() },
        { "Many lights",    sett->GetManyLights() },
        { "V-Sync",         GRAPHICS->GetVSync() },
        { "Fullscreen",     GRAPHICS->GetFullscreen() },
        { "Resolution",     GRAPHICS->GetSize() }
    };

    graphicsPage_ = armature_->CreateChild<SettingsPage>();
    graphicsPage_->AddSettings(graphicsSettings_);
}

void MenuMaster::CreateAudioPage()
{
    Settings* sett{ GetSubsystem<Settings>() };
    const Vector<Setting> audioSettings_{
        { "Effects",    sett->GetEffectsOn() },
        { "Effects",    sett->GetEffectsGain() },
        { "Music",      sett->GetMusicOn() },
        { "Music",      sett->GetMusicGain() }
    };

    audioPage_ = armature_->CreateChild<SettingsPage>();
    audioPage_->AddSettings(audioSettings_);
}

void MenuMaster::CreateInputPage()
{
    Vector<Setting> inputSettings_{};
    inputPage_ = armature_->CreateChild<SettingsPage>();
    inputPage_->AddSettings(inputSettings_);
}

void MenuMaster::CreatePageButtons()
{
    UIElement* pageNameRow{ armature_->CreateChild<UIElement>() };
    pageNameRow->SetAlignment(HA_CENTER, VA_CENTER);
    pageNameRow->SetPosition(0, -432);
    for (unsigned i{ 0 }; i < pageNames_.Size(); ++i)
    {
        Button* pageButton{ pageNameRow->CreateChild<Button>() };
        pageButton->SetAlignment(HA_CENTER, VA_CENTER);
        pageButton->SetFocusMode(FM_NOTFOCUSABLE);
        pageButton->SetBlendMode(BLEND_PREMULALPHA);
        pageButton->SetPosition(164 * (i - 1), 0);
        pageButton->SetTexture(RES(Texture2D, "UI/PageButton.png"));
        pageButton->SetImageRect({ 0, 0, 256, 128 });
        pageButton->SetHoverOffset(0, 128);
        pageButton->SetImageBorder({ 48, 24, 48, 24 });
        pageButton->SetBorder({ 8, 4, 8, 4 });
        pageButton->SetSize(128, 32);
        pageButton->SetOpacity(.9f);
        pageButton->SetVar("Page", Variant{ i });
        SubscribeToEvent(pageButton, E_CLICKEND, DRY_HANDLER(MenuMaster, HandlePageButtonPushed));

        Text* pageName{ pageButton->CreateChild<Text>() };
        pageName->SetFont(FONT_RABBIT);
        pageName->SetFontSize(17);
        pageName->SetColor(Color::AZURE);
        pageName->SetAlignment(HA_CENTER, VA_CENTER);
        pageName->SetTextEffect(TE_STROKE);
        pageName->SetEffectColor(Color::CYAN.Transparent(.5f));
        pageName->SetOpacity(.5f);
        pageName->SetText(pageNames_.At(i));

        pageButtons_.Push(pageButton);
    }
}

void MenuMaster::CreatePageArrows(const IntVector2& screenSize)
{
    for (bool right: { false, true })
    {
        Button* pageArrow{ armature_->CreateChild<Button>() };
        pageArrow->SetFocusMode(FM_NOTFOCUSABLE);
        pageArrow->SetTexture(RES(Texture2D, "UI/PageArrow.png"));
        pageArrow->SetImageRect({ right * 256, 0, 256 * (1 + right), 1024});
        pageArrow->SetHoverOffset(0, 1024);
        const int arrowWidth{ screenSize.y_ / 12};
        pageArrow->SetSize(arrowWidth, arrowWidth * 4);
        pageArrow->SetAlignment(HA_CENTER, VA_CENTER);
        pageArrow->SetBlendMode(BLEND_ALPHA);
        pageArrow->SetPosition(392 * (-1 + 2 * right), 0);
        pageArrow->SetVar("Page", Variant{ (right ? "Next" : "Previous") });
        SubscribeToEvent(pageArrow, E_CLICKEND, DRY_HANDLER(MenuMaster, HandlePageButtonPushed));
    }
}

void MenuMaster::Show()
{
    visible_ = true;

    GetSubsystem<Input>()->SetMouseMode(MM_ABSOLUTE);
    Highest::Get()->Fade(true);

    ValueAnimation* fadeAnim{ new ValueAnimation(context_) };
    fadeAnim->SetValueType(VAR_FLOAT);
    fadeAnim->SetKeyFrame(0.f, shade_->GetOpacity());
    fadeAnim->SetKeyFrame(1.f, 1.f );
    Cursor* cursor{ GetSubsystem<UI>()->GetCursor() };
    cursor->SetAttributeAnimation("Opacity", fadeAnim, WM_ONCE, 5.5f);
    shade_->SetAttributeAnimation("Opacity", fadeAnim, WM_ONCE, 2.3f);

    const IntVector2 pos{ armature_->GetPosition() };
    const IntVector2 targetPos{ ARMPOS_X, pos.y_ };
    ValueAnimation* slideAnim{ new ValueAnimation(context_) };
    slideAnim->SetValueType(VAR_INTVECTOR2);
    const int res{ 8 };
    const float dt{ 1.f / res };
    for (int i{ 0 }; i <= res; ++i)
    {
        const float t{ i * dt };
        slideAnim->SetKeyFrame(1.f - t, VectorRoundToInt(Vector2{ targetPos }.Lerp(Vector2{ pos }, t * t)));
    }

    armature_->SetAttributeAnimation("Position", slideAnim, WM_ONCE, 4.2f);
    TurnPage();
}

void MenuMaster::Hide()
{
    visible_ = false;

    GetSubsystem<Input>()->SetMouseMode(MM_RELATIVE);
    Highest::Get()->Fade(false);

    ValueAnimation* fadeAnim{ new ValueAnimation(context_) };
    fadeAnim->SetValueType(VAR_FLOAT);
    fadeAnim->SetKeyFrame(0.f, shade_->GetOpacity());
    fadeAnim->SetKeyFrame(1.f, .0f);
    Cursor* cursor{ GetSubsystem<UI>()->GetCursor() };
    cursor->SetAttributeAnimation("Opacity", fadeAnim, WM_ONCE, 5.5f);
    shade_->SetAttributeAnimation("Opacity", fadeAnim, WM_ONCE, 1.7f);

    const IntVector2 rootSize{ armature_->GetRoot()->GetSize() };
    const IntVector2 pos{ armature_->GetPosition() };
    const IntVector2 targetPos{ rootSize.x_, pos.y_ };
    ValueAnimation* slideAnim{ new ValueAnimation(context_) };
    slideAnim->SetValueType(VAR_INTVECTOR2);
    const int res{ 8 };
    const float dt{ 1.f / res };
    for (int i{ 0 }; i <= res; ++i)
    {
        const float t{ i * dt };
        slideAnim->SetKeyFrame(t, VectorRoundToInt(Vector2{ pos }.Lerp(Vector2{ targetPos }, t * t)));
    }

    armature_->SetAttributeAnimation("Position", slideAnim, WM_ONCE, 2.3f);
}

void MenuMaster::NextPage()
{
    const unsigned time{ TIME->GetSystemTime() };
    if (lastStep_.x_ > 0 && time - lastStepped_ < 42u)
    {
        lastStepped_ = time;
        return;
    }

    if (++currentPage_ >= pages_.Size())
        currentPage_ = 0;

    lastStep_ = { 1, lastStep_.y_ };
    TurnPage();
}

void MenuMaster::PreviousPage()
{
    const unsigned time{ TIME->GetSystemTime() };
    if (lastStep_.x_ < 0 && time - lastStepped_ < 42u)
    {
        lastStepped_ = time;
        return;
    }

    if (currentPage_ == 0)
        currentPage_ = pages_.Size() - 1;
    else
        --currentPage_;

    lastStep_ = { -1, lastStep_.y_ };
    TurnPage();
}

void MenuMaster::SetPage(unsigned page)
{
    if (page == currentPage_)
        return;

    currentPage_ = page;
    TurnPage();
}

void MenuMaster::NextElement()
{
    const unsigned time{ TIME->GetSystemTime() };
    if (lastStep_.y_ < 0 && time - lastStepped_ < 200u)
        return;
    lastStepped_ = time;
    lastStep_ = { lastStep_.x_, -1 };;

    UI* ui{ GetSubsystem<UI>() };
    PODVector<UIElement*> focusElems{ pages_.At(currentPage_)->GetFocusableElements() };
    unsigned focusIndex{ focusElems.IndexOf(ui->GetFocusElement()) };
    if (focusIndex < focusElems.Size())
        ui->SetFocusElement(focusElems.At(++focusIndex % focusElems.Size()));
    else if (!focusElems.IsEmpty())
        ui->SetFocusElement(focusElems.Front());
    else
        ui->SetFocusElement(nullptr);
}

void MenuMaster::PreviousElement()
{
    const unsigned time{ TIME->GetSystemTime() };
    if (lastStep_.y_ > 0 && time - lastStepped_ < 200u)
        return;
    lastStepped_ = time;
    lastStep_ = { lastStep_.x_, 1 };

    UI* ui{ GetSubsystem<UI>() };
    PODVector<UIElement*> focusElems{ pages_.At(currentPage_)->GetFocusableElements() };
    unsigned focusIndex{ focusElems.IndexOf(ui->GetFocusElement()) };
    if (focusIndex < focusElems.Size())
        ui->SetFocusElement(focusElems.At(focusIndex == 0 ? focusElems.Size() - 1 : --focusIndex));
    else if (!focusElems.IsEmpty())
        ui->SetFocusElement(focusElems.Back());
    else
        ui->SetFocusElement(nullptr);
}

void MenuMaster::Increment(float amount)
{
    UI* ui{ GetSubsystem<UI>() };
    UIElement* focusElem{ ui->GetFocusElement() };
    if (focusElem && focusElem->GetType() == Slider::GetTypeStatic())
    {
        Slider* slider{ static_cast<Slider*>(focusElem) };
        slider->SetValue(slider->GetValue() + amount);
    }
}

void MenuMaster::Confirm()
{
    const unsigned time{ TIME->GetSystemTime() };
    if (time - lastStepped_ < 111u)
    {
        lastStepped_ = time;
        return;
    }

    lastStepped_ = time;

    UI* ui{ GetSubsystem<UI>() };
    UIElement* focusElem{ ui->GetFocusElement() };
    if (focusElem)
        focusElem->OnKey(KEY_SPACE, MOUSEB_NONE, QUAL_NONE);
}

void MenuMaster::TurnPage()
{
    lastStepped_ = TIME->GetSystemTime();
    title_->SetText(pageNames_.At(currentPage_));

    for (unsigned p{ 0 }; p < pages_.Size(); ++p)
    {
        const bool c{ p == currentPage_ };
        Button* pageButton{ pageButtons_.At(p) };
        pageButton->SetImageRect({ 0, 128 * c, 256, 128 * (c + 1) });
        pageButton->SetHoverOffset(0, 128 * !c);

        UIElement* page{ pages_.At(p) };

        if (page)
            page->SetVisible(p == currentPage_);
    }

    UI* ui{ GetSubsystem<UI>() };
    PODVector<UIElement*> focusElems{ pages_.At(currentPage_)->GetFocusableElements() };
    ui->SetFocusElement(focusElems.IsEmpty() ? nullptr : focusElems.Front());
}

void MenuMaster::HandleMenuAction(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    const unsigned time{ TIME->GetSystemTime() };

    if (time - lastToggled_ > 42u)
        SetVisible(!visible_);

    lastToggled_ = time;
}

void MenuMaster::HandleEnterLobby(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    lastToggled_ = TIME->GetSystemTime();
}

void MenuMaster::HandleScreenMode(StringHash /*eventType*/, VariantMap& eventData)
{
    GetSubsystem<UI>()->SetScale(eventData[ScreenMode::P_HEIGHT].GetInt() / 1080.f);
    const IntVector2 pos{ ARMPOS_X, armature_->GetPosition().y_ };
    armature_->SetPosition(pos);
}

void MenuMaster::HandlePageButtonPushed(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    const Variant pageVar{ static_cast<UIElement*>(GetEventSender())->GetVar("Page") };

    if (pageVar.GetType() != VAR_STRING)
        SetPage(pageVar.GetUInt());
    else
        pageVar.GetString().StartsWith("N") ? NextPage()
                                            : PreviousPage();
}
