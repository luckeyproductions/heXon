/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"

#include "../inputmaster.h"
#include "../player/pilot.h"
#include "../player/player.h"

#include "door.h"

Door* Door::instance_{ nullptr };

void Door::RegisterObject(Context *context)
{
    context->RegisterFactory<Door>();
    context->MC->GetSample("Door");
}

Door::Door(Context* context): LogicComponent(context),
    hasBeenOpen_{ false },
    open_{ false }
{
    instance_ = this;

    SetUpdateEventMask(USE_UPDATE);
}

void Door::Start()
{
    model_ = node_->CreateComponent<AnimatedModel>();
    model_->SetModel(RES(Model, "Models/Door.mdl"));
    model_->SetMaterial(0, RES(Material, "Materials/Basic.xml"));
    model_->SetCastShadows(true);

    Node* lightNode{ node_->CreateChild("DoorLight") };
    lightNode->LookAt(Vector3::BACK);
    lightNode->SetPosition(Vector3{ 0.f, 1., 3.4f });
    Light* doorLight{ lightNode->CreateComponent<Light>() };
    doorLight->SetLightType(LIGHT_SPOT);
    doorLight->SetFov(160.f);
    doorLight->SetRange(11.f);
    doorLight->SetBrightness(6.66f);
    doorLight->SetSpecularIntensity(.05f);
    doorLight->SetCastShadows(true);
    doorLight->SetShadowBias(BiasParameters(.0000023f, 2.3f));

    node_->CreateComponent<SoundSource>();

    RigidBody* triggerBody{ node_->CreateComponent<RigidBody>() };
    triggerBody->SetTrigger(true);
    CollisionShape* trigger{ node_->CreateComponent<CollisionShape>() };
    trigger->SetBox(Vector3(3.4f, 2.3f, 1.f));

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Door, Open));
    SubscribeToEvent(node_, E_NODECOLLISIONEND, DRY_HANDLER(Door, Close));

}

void Door::Open(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!open_)
    {
        node_->GetComponent<SoundSource>()->Play(MC->GetSample("Door"));
        open_ = true;
    }
}
void Door::Close(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    PODVector<RigidBody*> colliders{};
    node_->GetComponent<RigidBody>()->GetCollidingBodies(colliders);

    if (!colliders.Size() && open_)
    {
        node_->GetComponent<SoundSource>()->Play(MC->GetSample("Door"));
        open_ = false;
        hasBeenOpen_ = true;
    }
}

bool Door::HidesAllPilots(bool onlyHuman) const
{
    if (!hasBeenOpen_)
        return false;

    Vector<Controllable*> controllables{ GetSubsystem<InputMaster>()->GetControlled() };

    for (Controllable* c: controllables)
    {
        if (c->IsInstanceOf<Pilot>())
        {
            if ((c->GetWorldPosition().z_ < node_->GetPosition().z_ + .333f) &&
                (c->GetPlayer()->IsHuman() || !onlyHuman))
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    return model_->GetMorphWeight(0) < .0023f || onlyHuman;
}

void Door::Update(float timeStep)
{

    model_->SetMorphWeight(0, Lerp( model_->GetMorphWeight(0),
                                  static_cast<float>(open_),
                                  timeStep * 7.f) );
}
