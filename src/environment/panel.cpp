/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../effectmaster.h"
#include "../enemy/razor.h"
#include "../player/pilot.h"
#include "../player/player.h"

#include "panel.h"

void Panel::RegisterObject(Context* context)
{
    context->RegisterFactory<Panel>();
}

Panel::Panel(Context* context): LogicComponent(context),
    infoNodes_{},
    currentInfoNode_{ nullptr },
    currentInfoNodeIndex_{ 0u },
    sinceInfoChange_{ 0.f },
    active_{ false }
{
}

void Panel::Initialize(int colorSet)
{
    colorSet_ = colorSet;

    CreatePanels();
    FadeOutPanel(true);

    SubscribeToEvent(E_ENTERLOBBY, DRY_HANDLER(Panel, EnterLobby));
    SubscribeToEvent(E_ENTERPLAY,  DRY_HANDLER(Panel, EnterPlay));
}

void Panel::CreatePanels()
{
    panelScene_ = new Scene{ context_ };
    panelScene_->CreateComponent<Octree>();

    Zone* panelZone{ panelScene_->CreateComponent<Zone>() };
    panelZone->SetFogColor(Color::WHITE);
    panelZone->SetFogStart(5.5f);
    panelZone->SetFogEnd(10.f);

    Node* panelCamNode{ panelScene_->CreateChild("Camera") };
    panelCamNode->SetPosition(Vector3::BACK * 5.f);
    Camera* panelCam{ panelCamNode->CreateComponent<Camera>() };

    CreateInfos();

    panelTexture_ = new Texture2D{ context_ };
    panelTexture_->SetSize(1024, 1024, GRAPHICS->GetRGBFormat(), TEXTURE_RENDERTARGET);

    RenderSurface* panelSurface{ panelTexture_->GetRenderSurface() };
    SharedPtr<Viewport> panelViewport{ new Viewport{ context_, panelScene_, panelCam } };
    panelSurface->SetViewport(0, panelViewport);

    for (bool small: { true, false })
    {
        Vector3 panelPos{};
        Quaternion panelRot{};

        switch(colorSet_)
        {
        case 1:
        {
            panelPos = small ? Vector3{  -.83787f, .4306f, 2.01268f }
                             : Vector3{ -4.49769f, .0f, 2.59509f };

            panelRot = small ? Quaternion{ -120.f, Vector3::UP }
                             : Quaternion{  -60.f, Vector3::UP };
        } break;
        case 2:
        {
            panelPos = small ? Vector3{  .83787f, .4306f, 2.01268f }
                             : Vector3{ 4.49769f, .0f,    2.59509f };
            panelRot = small ? Quaternion{ 120.f, Vector3::UP }
                             : Quaternion{  60.f, Vector3::UP };
        } break;
        case 3:
        {
            panelPos = small ? Vector3{ -3.46253f, .4306f, -.47611f }
                             : Vector3{ -4.49767f, .0f,   -2.59507f };
            panelRot = small ? Quaternion{  120.f, Vector3::UP }
                             : Quaternion{ -120.f, Vector3::UP };
        } break;
        case 4:
        {
            panelPos = small ? Vector3{ 3.46253f, .4306f, -.47611f }
                             : Vector3{ 4.49767f, .0f, -2.59507f };
            panelRot = small ? Quaternion{ -120.f, Vector3::UP }
                             : Quaternion{  120.f, Vector3::UP };
        } break;
        default: break;
        }

        Node* panelNode{ node_->CreateChild(small ? "SmallPanel" : "BigPanel") };
        panelNode->SetPosition(panelPos);
        panelNode->SetRotation(panelRot);
        panelNode->SetScale(small ? 1.f : 3.472769409f);
        panelNode->SetEnabled(small);

        StaticModel* panelModel{ panelNode->CreateComponent<StaticModel>() };
        panelModel->SetModel(RES(Model, "Models/Panel.mdl"));

        SharedPtr<Material> panelMaterial{};

        if (small)
        {
            panelMaterial = MC->colorSets_[colorSet_].panelMaterial_->Clone();
            panelMaterial->SetTexture(TU_EMISSIVE, panelTexture_);

            smallPanelNode_ = panelNode;

            panelTriggerNode_ = node_->CreateChild("PanelTrigger");
            panelTriggerNode_->SetPosition(panelPos);
            panelTriggerNode_->CreateComponent<RigidBody>()->SetTrigger(true);
            CollisionShape* panelTrigger{ panelTriggerNode_->CreateComponent<CollisionShape>() };
            panelTrigger->SetBox({ .7f, .9f, 1.23f });

            SubscribeToEvent(panelTriggerNode_, E_NODECOLLISIONSTART, DRY_HANDLER(Panel, ActivatePanel));
            SubscribeToEvent(panelTriggerNode_, E_NODECOLLISIONEND, DRY_HANDLER(Panel, DeactivatePanel));

        }
        else
        {
            panelMaterial = MC->colorSets_[colorSet_].addMaterial_->Clone();
            panelMaterial->SetTexture(TU_DIFFUSE, panelTexture_);

            bigPanelNode_ = panelNode;
        }

        panelModel->SetMaterial(panelMaterial);
    }
}

void Panel::CreateInfos()
{
    Node* infos{ panelScene_->CreateChild("Infos") };
    infos->Rotate({ 180.f * ((colorSet_ + 1) % 2), Vector3::FORWARD });

    // Create Apple info
    Node* appleInfo{ infos->CreateChild("AppleInfo") };
    infoNodes_.Push(appleInfo);

    Node* apple{ appleInfo->CreateChild("PanelApple") };
    apple->SetPosition(Vector3::RIGHT + Vector3::FORWARD);
    apple->Rotate({ 180.f * ((colorSet_ + 1) % 2), Vector3::FORWARD });
    apple->SetScale(.34f);
    apple->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/Apple.mdl"));

    Node* appleEqualsNode{ appleInfo->CreateChild("SpireEquals") };
    appleEqualsNode->Rotate({  90.f, Vector3::LEFT });
    appleEqualsNode->Rotate({ 180.f, Vector3::UP });
    appleEqualsNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/=.mdl"));

    Node* appleScoreNode{ appleInfo->CreateChild("AppleScore") };
    appleScoreNode->SetPosition(Vector3::LEFT * .75f);
    appleScoreNode->Rotate({  90.f, Vector3::LEFT });
    appleScoreNode->Rotate({ 180.f, Vector3::UP });
    appleScoreNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/2.mdl"));
    Node* appleThreeNode{ appleScoreNode->CreateChild("3") };
    appleThreeNode->SetPosition(Vector3::RIGHT * .5f);
    appleThreeNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/3.mdl"));

    // Create Heart info
    Node* heartInfo{ infos->CreateChild("HeartInfo") };
    infoNodes_.Push(heartInfo);

    Node* heart{ heartInfo->CreateChild("PanelHeart") };
    heart->SetPosition(Vector3::RIGHT + Vector3::FORWARD);
    heart->Rotate({ 180.f * ((colorSet_ + 1) % 2), Vector3::FORWARD });
    heart->SetScale(.34f);
    heart->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/Heart.mdl"));

    Node* heartEqualsNode{ heartInfo->CreateChild("HeartEquals") };
    heartEqualsNode->Rotate({  90.f, Vector3::LEFT });
    heartEqualsNode->Rotate({ 180.f, Vector3::UP });
    heartEqualsNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/=.mdl"));

    Node* heartScoreNode{ heartInfo->CreateChild("HeartScore") };
    heartScoreNode->SetPosition(Vector3::LEFT);
    heartScoreNode->Rotate({  90.f, Vector3::LEFT });
    heartScoreNode->Rotate({ 180.f, Vector3::UP });
    heartScoreNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/0.mdl"));

    // Create Razor info
    Node* razorInfo{ infos->CreateChild("RazorInfo") };
    infoNodes_.Push(razorInfo);

    Node* razor{ razorInfo->CreateChild("PanelRazor") };
    razor->SetPosition(Vector3::RIGHT + Vector3::FORWARD);
    razor->SetScale(0.34f);
    razor->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/Core.mdl"));
    razor->CreateChild("RazorTop")->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/RazorHalf.mdl"));
    Node* razorBottomNode{ razor->CreateChild("RazorBottom") };
    razorBottomNode->Rotate({ 180.f, Vector3::RIGHT });
    razorBottomNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/RazorHalf.mdl"));

    Node* razorEqualsNode{ razorInfo->CreateChild("RazorEquals") };
    razorEqualsNode->Rotate({  90.f, Vector3::LEFT });
    razorEqualsNode->Rotate({ 180.f, Vector3::UP });
    razorEqualsNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/=.mdl"));

    Node* razorScoreNode{ razorInfo->CreateChild("RazorScore") };
    razorScoreNode->SetPosition(Vector3::LEFT);
    razorScoreNode->Rotate({  90.f, Vector3::LEFT });
    razorScoreNode->Rotate({ 180.f, Vector3::UP });
    razorScoreNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/5.mdl"));

    // Create Spire info
    Node* spireInfo{ infos->CreateChild("SpireInfo") };
    infoNodes_.Push(spireInfo);

    Node* spire{ spireInfo->CreateChild("PanelSpire") };
    spire->SetPosition(Vector3::RIGHT + Vector3::FORWARD);
    spire->Rotate({ 180.f * ((colorSet_ + 1) % 2), Vector3::FORWARD });
    spire->SetScale(.34f);
    spire->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/Core.mdl"));
    spire->CreateChild("SpireTop")   ->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/SpireTop.mdl"));
    spire->CreateChild("SpireBottom")->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/SpireBottom.mdl"));

    Node* spireEqualsNode{ spireInfo->CreateChild("SpireEquals") };
    spireEqualsNode->Rotate({  90.f, Vector3::LEFT });
    spireEqualsNode->Rotate({ 180.f, Vector3::UP });
    spireEqualsNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/=.mdl"));

    Node* spireScoreNode{ spireInfo->CreateChild("SpireScore") };
    spireScoreNode->SetPosition(Vector3::LEFT * .75f);
    spireScoreNode->Rotate({  90.f, Vector3::LEFT });
    spireScoreNode->Rotate({ 180.f, Vector3::UP });
    spireScoreNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/1.mdl"));
    Node* spireZeroNode{ spireScoreNode->CreateChild("0") };
    spireZeroNode->SetPosition(Vector3::RIGHT * .5f);
    spireZeroNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/0.mdl"));

    // Create Mason info
    Node* masonInfo{ infos->CreateChild("MasonInfo") };
    infoNodes_.Push(masonInfo);

    Node* mason{ masonInfo->CreateChild("PanelMason") };
    mason->SetPosition(Vector3::RIGHT + Vector3::FORWARD);
    mason->Rotate({ 180.f * ((colorSet_ + 1) % 2), Vector3::FORWARD });
    mason->SetScale(.34f);
    mason->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/Core.mdl"));
    mason->CreateChild("MasonTop")   ->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/MasonTop.mdl"));
    mason->CreateChild("MasonBottom")->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/MasonBottom.mdl"));

    Node* masonEqualsNode{ masonInfo->CreateChild("MasonEquals") };
    masonEqualsNode->Rotate({  90.f, Vector3::LEFT });
    masonEqualsNode->Rotate({ 180.f, Vector3::UP });
    masonEqualsNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/=.mdl"));

    Node* masonScoreNode{ masonInfo->CreateChild("MasonScore") };
    masonScoreNode->SetPosition(Vector3::LEFT * .75f);
    masonScoreNode->Rotate({  90.f, Vector3::LEFT });
    masonScoreNode->Rotate({ 180.f, Vector3::UP });
    masonScoreNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/4.mdl"));
    Node* masonTwoNode{ masonScoreNode->CreateChild("0") };
    masonTwoNode->SetPosition(Vector3::RIGHT * .5f);
    masonTwoNode->CreateComponent<StaticModel>()->SetModel(RES(Model, "Models/2.mdl"));

    // Set first info as current
    SetCurrentInfoNode(infoNodes_.At(currentInfoNodeIndex_));
}

void Panel::Update(float timeStep)
{
    panelScene_->GetChild("RazorTop",    true)->Rotate({ timeStep * 42.f, Vector3::UP });
    panelScene_->GetChild("RazorBottom", true)->Rotate({ timeStep * 34.f, Vector3::UP });

    panelScene_->GetChild("SpireTop",    true)->Rotate({ timeStep * 42.f, Vector3::UP });
    panelScene_->GetChild("SpireBottom", true)->Rotate({ timeStep * 34.f, Vector3::DOWN });

    panelScene_->GetChild("MasonTop",    true)->Rotate({ timeStep * 64.f, Vector3::UP });
    panelScene_->GetChild("MasonBottom", true)->Rotate({ timeStep * 55.f, Vector3::DOWN });

    panelScene_->GetChild("PanelApple", true)->Rotate({ timeStep * 64.f, Vector3::UP });
    panelScene_->GetChild("PanelHeart", true)->Rotate({ timeStep * 55.f, Vector3::UP });

    if (active_)
    {
        sinceInfoChange_ += timeStep;

        if (sinceInfoChange_ > 4.f)
        {
            sinceInfoChange_ = 0.f;
            ++currentInfoNodeIndex_;

            if (currentInfoNodeIndex_ == infoNodes_.Size())
                currentInfoNodeIndex_ = 0;

            SetCurrentInfoNode(infoNodes_.At(currentInfoNodeIndex_));
        }
    }
}

void Panel::SetCurrentInfoNode(Node* infoNode)
{
    currentInfoNode_ = infoNode;

    for (Node* n: infoNodes_)
        GetSubsystem<EffectMaster>()->TranslateTo(n, Vector3::FORWARD * 13.f * (n != infoNode), .42f);
}

void Panel::EnterLobby(StringHash /*eventType*/, VariantMap&/*eventData*/)
{
    smallPanelNode_->SetEnabled(true);
    bigPanelNode_->SetEnabled(true);
    panelTriggerNode_->SetEnabled(true);
    node_->SetEnabledRecursive(true);
}

void Panel::EnterPlay(StringHash /*eventType*/, VariantMap&/*eventData*/)
{
    node_->SetEnabledRecursive(false);
}

void Panel::ActivatePanel(StringHash /*eventType*/, VariantMap &eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };

    if (Pilot* pilot{ otherNode->GetComponent<Pilot>() })
    {
        int pilotColorSet{};
        Player::takenColorSets_.TryGetValue(pilot->GetPlayerId(), pilotColorSet);

        if (IsOwner(pilot->GetPlayerId()) || (!HasOwner() && !pilotColorSet))
        {
            FadeInPanel();
            active_ = true;
        }
    }
}

void Panel::FadeInPanel()
{
    GetSubsystem<EffectMaster>()->FadeTo(bigPanelNode_->GetComponent<StaticModel>()->GetMaterial(),
                                         MC->colorSets_[colorSet_].addMaterial_->GetShaderParameter("MatDiffColor").GetColor(),
                                         .23f, .1f);
    GetSubsystem<EffectMaster>()->FadeTo(smallPanelNode_->GetComponent<StaticModel>()->GetMaterial(),
                                         MC->colorSets_[colorSet_].glowMaterial_->GetShaderParameter("MatEmissiveColor").GetColor(),
                                         .23f, 0.f, "MatEmissiveColor");
}

void Panel::DeactivatePanel(StringHash /*eventType*/, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };

    if (Pilot* pilot{ otherNode->GetComponent<Pilot>() })
    {
        int pilotColorSet{};
        Player::takenColorSets_.TryGetValue(pilot->GetPlayerId(), pilotColorSet);

        PODVector<RigidBody*> bodies{};
        panelTriggerNode_->GetComponent<RigidBody>()->GetCollidingBodies(bodies);

        for (RigidBody* body: bodies)
        {
            if (Pilot* otherPilot{ body->GetNode()->GetComponent<Pilot>() })
            {
                int otherPilotColorSet{};

                if (otherPilot == pilot || Player::takenColorSets_.TryGetValue(otherPilot->GetPlayerId(), otherPilotColorSet))
                    bodies.Remove(body);
            }
            else
            {
                bodies.Remove(body);
            }
        }

        if (IsOwner(pilot->GetPlayerId()) || (!HasOwner() && !bodies.Size()))
        {
            FadeOutPanel();
            active_ = false;
        }
    }
}
void Panel::FadeOutPanel(bool immediate)
{
    GetSubsystem<EffectMaster>()->FadeTo(bigPanelNode_->GetComponent<StaticModel>()->GetMaterial(),
                                         Color::BLACK,
                                         .23f * !immediate, .1f * !immediate);
    GetSubsystem<EffectMaster>()->FadeTo(smallPanelNode_->GetComponent<StaticModel>()->GetMaterial(),
                                         Color::BLACK,
                                         .23f * !immediate, .1f * !immediate, "MatEmissiveColor");
}

bool Panel::IsOwner(int playerId)
{
    for (int p: Player::takenColorSets_.Keys())
    {
        if (playerId == p && Player::takenColorSets_[p] == colorSet_)
            return true;
    }

    return false;
}

bool Panel::HasOwner()
{
    if (Player::takenColorSets_.Values().Contains(colorSet_))
        return true;
    else
        return false;
}
