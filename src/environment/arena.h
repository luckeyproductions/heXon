/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ARENA_H
#define ARENA_H


#include "../luckey.h"

namespace Dry {
class Node;
}

class Tile;
class Slot;

enum TileElement {TE_CENTER = 0, TE_NORTH, TE_EAST, TE_SOUTH, TE_WEST, TE_NORTHWEST, TE_NORTHEAST, TE_SOUTHEAST, TE_SOUTHWEST, TE_LENGTH};
enum TileType {B_SPACE, B_EMPTY, B_ENGINE};

#define ARENA_SIZE 23
#define ARENA_RADIUS Cosine(M_DEGTORAD * 30.f) * (ARENA_SIZE + 1.f)

const float ALPHASCALE{ 16.f };
const float ALPHASCALE_INV{ 1.f / ALPHASCALE };

class Arena: public LogicComponent
{
    DRY_OBJECT(Arena, LogicComponent);

public:
    static void RegisterObject(Context* context);
    Arena(Context* context);

    void Start() override;
    void Update(float timeStep) override;

    void AddToAffectors(Node* affector);
    void RemoveFromAffectors(Node* affector);
    static const PODVector<Pair<Vector3, float> >& GetEffectVector() { return effectVector_; }

    Tile* GetRandomTile(bool forMason = false);
    void FlashX(const Color& color);

    Vector3 ForceFieldAt(const Vector3& position);

protected:
    void FixedPostUpdate(float timeStep) override;

private:
    void EnterPlay(StringHash, VariantMap&);
    void EnterLobby(StringHash, VariantMap&);
    void UpdateEffectVector(StringHash, VariantMap&);

    Color VectorToColor(const Vector3& vec)
    {
        const Vector3 normVec{ .5f * (vec.Normalized() + Vector3::ONE)  };

        return Color{ normVec.x_,
                      normVec.y_,
                      normVec.z_,
                      Min(1.f, vec.Length() * ALPHASCALE_INV) };
    }
    Vector3 ColorToVector(const Color& col)
    {
        const float scaledAlpha{ col.a_ * ALPHASCALE };

        return Vector3{ scaledAlpha * Vector3{ col.r_ - .5f,
                                               col.g_ - .5f,
                                               col.b_ - .5f } };
    }

    Vector3 targetPosition_;
    Vector3 targetScale_;
    Node* logoNode_;
    Material* logoMaterial_;
    Material* xMaterial_;
    Light* playLight_;
    HashSet<Node*> hexAffectors_;
    Vector<Tile*> tiles_;
    static PODVector<Pair<Vector3, float> > effectVector_;

    SharedPtr<Image> forceField_;
    void UpdateMassField();
    float sinceFieldUpdate_;
};

#endif // ARENA_H
