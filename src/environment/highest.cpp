/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../player/pilot.h"
#include "../ui/menumaster.h"

#include "highest.h"

Highest* Highest::instance_{ nullptr };

void Highest::RegisterObject(Context* context)
{
    context->RegisterFactory<Highest>();
}

Highest::Highest(Context* context): LogicComponent(context),
    highestScore_{ 0u }
{
    instance_ = this;
}

void Highest::Start()
{
    node_->SetPosition(HIGHEST_POS);
    node_->Rotate(Quaternion{  45.f, Vector3::RIGHT });
    node_->Rotate(Quaternion{ 180.f, Vector3::UP });
    Node* podiumNode{ node_->CreateChild("Podium") };
    podiumNode->SetScale(0.5f);
    StaticModel* hexPodium{ podiumNode->CreateComponent<StaticModel>() };
    hexPodium->SetModel(RES(Model, "Models/Hexagon.mdl"));
    hexPodium->SetMaterial(RES(Material, "Materials/BackgroundTile.xml")->Clone());
    Node* highestPilotNode{ podiumNode->CreateChild("HighestPilot") };
    highestPilotNode->SetScale(2.f);

    Node* spotNode{ node_->CreateChild("HighestSpot") };
    spotNode->Translate(Vector3{ 0.f, -2.f, 3.f });
    spotNode->LookAt(node_->GetWorldPosition());
    Light* highestSpot{ spotNode->CreateComponent<Light>() };
    highestSpot->SetLightType(LIGHT_SPOT);
    highestSpot->SetRange(5.f);
    highestSpot->SetFov(23.5f);
    highestSpot->SetColor(Color{ .6f, 0.7f, 1.0f });
    highestSpot->SetBrightness(5.f);
    highestSpot->SetSpecularIntensity(.23f);

    UI* ui{ GetSubsystem<UI>() };
    highestScoreText_ = ui->GetRoot()->CreateChild<Text>();
    highestScoreText_->SetName("HighestScore");
    highestScoreText_->SetText("0");

    highestScoreText_->SetFont(FONT_RABBIT, 23);
    highestScoreText_->GetFont()->SetScaledGlyphOffset({ 0, -.05f });

    highestScoreText_->SetColor(Color{ .23f, .75f, 1.f, 1.f });
    highestScoreText_->SetHorizontalAlignment(HA_CENTER);
    highestScoreText_->SetVerticalAlignment(VA_CENTER);
    highestScoreText_->SetPosition(0, ui->GetRoot()->GetHeight() / 4.2f);

    Node* pilotNode{ node_->CreateChild("HighestPilot") };
    highestPilot_ = pilotNode->CreateComponent<Pilot>();
    highestPilot_->Initialize(true);

    if (highestScore_ == 0)
    {
        node_->SetEnabledRecursive(false);
        highestScoreText_->SetColor(Color::TRANSPARENT_BLACK);
    }
    else
    {
        podiumNode->SetRotation(Quaternion::IDENTITY);
        podiumNode->Rotate(Quaternion(RandomSign() * 60.f, Vector3::UP));
        node_->SetEnabledRecursive(true);
        highestScoreText_->SetText(String(highestScore_));
        highestScoreText_->SetColor(Color{ .23f, .75f, 1.f, .75f });
    }

    SubscribeToEvent(E_ENTERLOBBY, DRY_HANDLER(Highest, EnterLobby));
    SubscribeToEvent(E_ENTERPLAY,  DRY_HANDLER(Highest, EnterPlay));
}
void Highest::Update(float timeStep)
{
    node_->SetPosition(HIGHEST_POS + Vector3::UP * MC->Sine(.1f, -.23f, 0.f));
}

void Highest::SetPilot(Pilot* pilot, unsigned score)
{
    if (score <= highestScore_)
        return;

    highestPilot_->Clone(pilot);

    SetScore(score);

    pilot->Save(0, highestScore_);

    if (highestScore_ > 0)
    {
        node_->SetEnabledRecursive(true);
        highestScoreText_->SetColor(Color{ .23f, .75f, 1.f, .75f });
    }
}

void Highest::SetScore(unsigned score)
{
    highestScore_ = score;
    highestScoreText_->SetText(String(highestScore_));
}

void Highest::Fade(bool out)
{
    ValueAnimation* fadeAnim{ new ValueAnimation(context_) };
    fadeAnim->SetValueType(VAR_FLOAT);
    fadeAnim->SetKeyFrame(0.f, highestScoreText_->GetOpacity());
    if (out)
        fadeAnim->SetKeyFrame(1.f, 0.f );
    else
        fadeAnim->SetKeyFrame(1.f, 1.f );

    highestScoreText_->SetAttributeAnimation("Opacity", fadeAnim, WM_ONCE, 5.f);
}

void Highest::EnterLobby(StringHash eventType, VariantMap& eventData)
{ (void)eventType; (void)eventData;

    node_->SetEnabledRecursive(highestScore_ != 0);
    highestScoreText_->SetColor(Color{ .23f, .75f, 1.f, .75f } * static_cast<float>(highestScore_ != 0));
}
void Highest::EnterPlay(StringHash eventType, VariantMap& eventData)
{ (void)eventType; (void)eventData;

    node_->SetEnabledRecursive(false);
    highestScoreText_->SetColor(Color{ .13f, .666f, 1.f, .23f } * static_cast<float>(highestScore_ != 0));
}
