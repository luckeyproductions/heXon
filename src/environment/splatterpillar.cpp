/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../player/pilot.h"

#include "splatterpillar.h"

SplatterPillar* SplatterPillar::instance_{};

void SplatterPillar::RegisterObject(Context *context)
{
    context->RegisterFactory<SplatterPillar>();

    for (int i{ 1 }; i < 6; ++i)
        context->MC->GetSample("Splatter" + String(i));
}

SplatterPillar::SplatterPillar(Context* context): LogicComponent(context),
    playerId_{ 0 },
    spun_{ false },
    reset_{ true },
    delay_{ .5f },
    delayed_{ 0.f },
    sequenceLength_{ 5.f },
    lastTriggered_{ -sequenceLength_ },
    rotationSpeed_{}
{
    instance_ = this;
}

void SplatterPillar::Start()
{
    playerId_ = node_->GetPosition().x_ < 0.f ? 1 : 2;

    node_->Rotate(Quaternion{ Random(6) * 60.f, Vector3::UP });
    pillarNode_ = node_->CreateChild("Pillar");
    bloodNode_ = node_->CreateChild("Blood");
    bloodNode_->SetScale(1.23f);
    pillar_ = pillarNode_->CreateComponent<AnimatedModel>();
    pillar_->SetModel(RES(Model, "Models/SplatterPillar.mdl"));
    pillar_->SetMorphWeight(0, 0.f);
    pillar_->SetCastShadows(true);

    pillar_->SetMaterial(0, RES(Material, "Materials/Basic.xml"));
    pillar_->SetMaterial(1, RES(Material, "Materials/BlueGlow.xml"));

    pillar_->SetMaterial(2, RES(Material, "Materials/Metal.xml"));
    pillar_->SetMaterial(3, RES(Material, "Materials/Drain.xml"));

    blood_ = bloodNode_->CreateComponent<AnimatedModel>();
    blood_->SetEnabled(false);
    blood_->SetCastShadows(true);
    blood_->SetModel(RES(Model, "Models/Blood.mdl"));
    blood_->SetMaterial(0, RES(Material, "Materials/Blood.xml")->Clone());

    particleNode_ = node_->CreateChild("BloodParticles");
    particleNode_->Translate(Vector3::UP * 2.3f);
    splatEmitter_ = particleNode_->CreateComponent<ParticleEmitter>();
    splatEmitter_->SetEffect(RES(ParticleEffect, "Particles/BloodSplat.xml"));
    splatEmitter_->SetEmitting(false);
    dripEmitter_ = particleNode_->CreateComponent<ParticleEmitter>();
    dripEmitter_->SetEffect(CACHE->GetTempResource<ParticleEffect>("Particles/BloodDrip.xml"));
    dripEmitter_->SetEmitting(false);

    soundSource_ = node_->CreateComponent<SoundSource>();

    RigidBody* triggerBody{ node_->CreateComponent<RigidBody>() };
    triggerBody->SetTrigger(true);
    CollisionShape* trigger{ node_->CreateComponent<CollisionShape>() };
    trigger->SetSphere(.13f, Vector3::UP * .42f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(SplatterPillar, Trigger));
}

void SplatterPillar::Trigger(StringHash /*eventType*/, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };

    if (otherNode->HasComponent<Pilot>())
    {
        otherNode->GetComponent<Pilot>()->Upload();

        rotationSpeed_ = RandomOffCenter();
        lastTriggered_ = MC->scene_->GetElapsedTime();
        bloodNode_->Rotate(Quaternion{ Random(360.f), Vector3::UP });
        blood_->SetEnabled(true);
        splatEmitter_->SetEmitting(true);
        soundSource_->Play(MC->GetSample("Splatter" + String(Random(1, 6))));
    }
}

void SplatterPillar::Update(float /*timeStep*/)
{
    if (MC->GetGameState() != GS_LOBBY) return;

    const float elapsedTime{ MC->scene_->GetElapsedTime() };
    const float intoSequence{ (elapsedTime - lastTriggered_) / sequenceLength_ };
    const unsigned numMorphs{ blood_->GetNumMorphs() };

    //Animate
    if (intoSequence < 1.f)
    {
        //Animate blood
        if (!bloodNode_->IsEnabled())
        {
            bloodNode_->SetEnabled(true);
            particleNode_->Rotate(Quaternion{ Random(360.f), Vector3::UP });
        }

        if (intoSequence > .023f)
        {
            splatEmitter_->SetEmitting(false);
            dripEmitter_->SetEmitting(true);
        }

        for (unsigned m{ 0 }; m < numMorphs; ++m)
        {
            float intoMorph{ Clamp(intoSequence * numMorphs - m, 0.f, 2.f) };
            if (intoMorph > 1.f)
                intoMorph = Max(2.f - intoMorph, 0.f);
            else if (m == 0)
                intoMorph = Min(intoMorph * 5.f, 1.f);

            blood_->SetMorphWeight(m, Clamp(intoMorph, 0.f, 1.f));
        }

        blood_->GetMaterial()->SetShaderParameter("MatDiffColor", Color{ .23f, .32f, .32f, Clamp(1.f - (intoSequence - .75f) * 5.f, 0.f, 1.f) });
        blood_->GetMaterial()->SetShaderParameter("Dissolve", .75f * intoSequence + .23f);
        ParticleEffect* dripEffect{dripEmitter_->GetEffect()};
        dripEffect->SetEmitterSize(Vector3{ 1.f - intoSequence, 0.f, 1.f - intoSequence});
        dripEffect->SetMinEmissionRate(Max(123.f - 200.f * intoSequence, 0.f));
        dripEffect->SetMaxEmissionRate(Max(320.f - 340.f * intoSequence, 0.f));

        //Animate pillar
        if      (intoSequence < .125f)
        {
            pillar_->SetMorphWeight(0, Clamp(123.f * intoSequence, 0.f, 1.f));
        }
        else if (intoSequence < .1666f)
        {
            pillar_->SetMorphWeight(0, 1.f);
            if (!spun_)
            {
                pillarNode_->Rotate(Quaternion{ Random(6) * 60.f, Vector3::UP });
                spun_ = true;
            }
        }
        else if (intoSequence > (1.f / 6.f))
        {
            spun_ = false;
            const float weight{ Max(2.f * (1.f - 3.f * intoSequence), 0.f)};
            pillar_->SetMorphWeight(0, Clamp(weight * weight * weight, 0.f, 1.f));
        }
    }
    else
    {
    //When idle
        //Reset
        if (bloodNode_->IsEnabled())
        {
            bloodNode_->SetEnabled(false);
            dripEmitter_->SetEmitting(false);
        }

        if (pillar_->GetMorphWeight(0) != 0.f)
            pillar_->SetMorphWeight(0, 0.f);
    }
}

bool SplatterPillar::IsIdle() const
{
    return !bloodNode_->IsEnabled();
}
