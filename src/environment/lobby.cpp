/* heXon
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../effectmaster.h"
#include "../mastercontrol.h"
#include "../player/ship.h"
#include "door.h"
#include "highest.h"
#include "splatterpillar.h"

#include "lobby.h"

void Lobby::RegisterObject(Context *context)
{
    context->RegisterFactory<Lobby>();
}

Lobby::Lobby(Context* context): LogicComponent(context)
{
}

void Lobby::Start()
{
    Node* chamberNode{ node_->CreateChild("Chamber", LOCAL) };
    StaticModel* chamberModel{ chamberNode->CreateComponent<StaticModel>() };
    chamberModel->SetModel(RES(Model, "Models/Chamber.mdl"));
    chamberModel->SetMaterial(0, RES(Material, "Materials/Marble.xml"));
    chamberModel->SetMaterial(1, RES(Material, "Materials/PitchBlack.xml"));
    chamberModel->SetMaterial(2, RES(Material, "Materials/BlueGlowEnvmap.xml"));
    chamberModel->SetMaterial(3, RES(Material, "Materials/Drain.xml"));
    chamberModel->SetCastShadows(true);

    //Create collider
    node_->CreateComponent<RigidBody>();
    node_->CreateComponent<CollisionShape>()->SetTriangleMesh(RES(Model, "Models/Chamber_COLLISION.mdl"));
    node_->CreateComponent<CollisionShape>()->SetBox(Vector3{ 5.5f, 1.f, 1.f });
    node_->CreateComponent<Navigable>()->SetRecursive(false);

    //Create lights
    for (unsigned l{ 0u }; l < 7u; ++l)
    {
        Node* spotNode{ node_->CreateChild("SpotLight") };
        spotNode->LookAt(Vector3::DOWN);
        spotNode->SetPosition(Vector3::RIGHT * 5.f * (l != 0) + Vector3::UP * 2.3f);
        spotNode->RotateAround(Vector3::ZERO, Quaternion{ 60.f * l, Vector3::UP }, TS_WORLD);

        Light* spotLight{ spotNode->CreateComponent<Light>() };
        spotLight->SetLightType(LIGHT_SPOT);
        spotLight->SetRange(5.f);
        spotLight->SetFov(120.f + ((l == 0) * 40.f));
        spotLight->SetCastShadows(true);
        spotLight->SetShadowBias(BiasParameters{ .0001f, .001f });
        spotLight->SetShadowResolution(.5f);
    }

    //Create door and splatterpillar
    Node* doorNode{ node_->CreateChild("Door", LOCAL) };
    doorNode->SetPosition(Vector3{ 0.f, 0.f, 5.21843f });
    doorNode->CreateComponent<Door>();

    Node* splatterPillarNode{ node_->CreateChild("SplatterPillar", LOCAL) };
    splatterPillarNode->SetPosition(Vector3{ 0.f, 0.f, -4.36142 });
    splatterPillarNode->CreateComponent<SplatterPillar>();

    //Create highest
    Node* highestNode{ node_->CreateChild("Highest", LOCAL) };
    highest_ = highestNode->CreateComponent<Highest>();

    //Add pilot button
    Node* addButton{ node_->CreateChild("AddButton") };
    addButton->SetPosition(Vector3(2.34f, .23f, 5.f));
    addButton->CreateComponent<RigidBody>()->SetTrigger(true);
    addButton->CreateComponent<CollisionShape>()->SetSphere(.5f);
    SubscribeToEvent(addButton, E_NODECOLLISIONSTART, DRY_HANDLER(Lobby, AddButtonPressed));

    //Remove pilot button
    Node* removeButton{ node_->CreateChild("RemoveButton") };
    removeButton->SetPosition(Vector3(-2.34f, .23f, 5.f));
    removeButton->CreateComponent<RigidBody>()->SetTrigger(true);
    removeButton->CreateComponent<CollisionShape>()->SetSphere(.5f);
    SubscribeToEvent(removeButton, E_NODECOLLISIONSTART, DRY_HANDLER(Lobby, RemoveButtonPressed));

    SubscribeToEvent(E_ENTERLOBBY, DRY_HANDLER(Lobby, EnterLobby));
    SubscribeToEvent(E_ENTERPLAY,  DRY_HANDLER(Lobby, EnterPlay));
}

void Lobby::Update(float /*timeStep*/)
{
    PODVector<Node*> lightNodes{};
    node_->GetChildrenWithComponent<Light>(lightNodes);

    for (Node* lightNode: lightNodes)
    {
        lightNode->GetComponent<Light>()->SetBrightness(
                    MC->Sine(2.3f, .7f, 1.4f,
                             M_PI_4 * M_DEGTORAD * (lightNode->GetPosition().Angle(Vector3::BACK) )));
    }
}

void Lobby::EnterLobby(StringHash eventType, VariantMap &eventData)
{

    node_->SetEnabledRecursive(true);
}
void Lobby::EnterPlay(StringHash eventType, VariantMap &eventData)
{

    node_->SetEnabledRecursive(false);
}

void Lobby::AddButtonPressed(StringHash eventType, VariantMap &eventData)
{

    MC->AddPlayer();
}
void Lobby::RemoveButtonPressed(StringHash eventType, VariantMap &eventData)
{

    MC->RemoveAutoPilot();
}
